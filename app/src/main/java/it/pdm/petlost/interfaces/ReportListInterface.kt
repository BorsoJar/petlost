package it.pdm.petlost.interfaces

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.ReportListItem
import it.pdm.petlost.model.data.Stato


interface ReportListInterface {

    interface ModelReport :NotificationsInterface.Model{
        fun getReportsDataForList(id: Boolean) : List<ReportListItem>
        fun getCurrentUser(): String
        fun deleteReport(reportId : String)
        fun deleteListReportPresenter()
        fun filterReportBy(address: LatLng?, animale: Animale?, data: Long?, dataTo: Long?, range: Double, reportState: Stato?, context: Context): List<ReportListItem>
        fun deleteReportItemPresenter()
        fun checkAdmin(): Boolean
        fun updateReportState(reportId: String, state: Stato)

    }

    interface PresenterReportList {
        fun getReportsData(id: Boolean, context: Context) : List<ReportListItem>
        fun getCurrentUser(): String
        fun deletePresenter()
        fun filterBy(
            address: LatLng?,
            animale: Animale?,
            data: Long?,
            dataTo: Long?,
            range: Double,
            reportState: Stato?,
            context: Context
        ): List<ReportListItem>
        fun updateData(reportsDataForList: List<ReportListItem>)
    }


    interface PresenterReportItem: NotificationsInterface.presenter{
        fun deleteReportRequest(author : String)
        fun deleteReportItemPresenter()
        fun onDeleteSuccess()
        fun onDeleteFailed()
        fun checkAdmin(): Boolean
        fun updateReportState(reportId: String, state: Stato)
    }

    interface ViewReportItem: NotificationsInterface.NewReportListener{
        fun onDeleteSuccess()
        fun onDeleteFailed()
    }

    interface ViewReportList {
        fun updateView(reportsDataForList: List<ReportListItem>)
    }


}