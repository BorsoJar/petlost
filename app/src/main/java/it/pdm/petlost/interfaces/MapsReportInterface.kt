package it.pdm.petlost.interfaces

import android.content.Context
import it.pdm.petlost.model.data.ReportListItem
import it.pdm.petlost.model.data.ReportsMapsItem


interface MapsReportInterface {

    interface ModelReport {
        fun getReportsDataForItem(id : String, context: Context) : ReportListItem?
        fun deleteMapsPresenter()

    }

    interface PresenterReport {

        fun getReportsData(id : String, context: Context) : ReportListItem?
        fun deletePresenter()
        fun updateData(mtbList: MutableList<ReportsMapsItem>)

    }

    interface ViewReport {
        fun updateView(mtbList: MutableList<ReportsMapsItem>)
    }

}