package it.pdm.petlost.interfaces


interface Adapter {

    interface ItemClickInterface {
        fun onItemClick(position: Int)
    }

    interface InfoWindowInterface{

    }

}