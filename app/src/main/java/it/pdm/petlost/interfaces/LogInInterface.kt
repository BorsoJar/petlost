package it.pdm.petlost.interfaces

import com.google.firebase.auth.FirebaseUser
import java.lang.Exception

interface LogInInterface {

    interface ModelLogIn{
        fun logInRequest(email: String, pw: String)
        fun sendMailConfirmationAgain()
        fun resetPasswordRequest(email: String)
    }

    interface PresenterLogIn{
        fun checkUserInfoFormat(email: String, pw: String) : Int
        fun tryToLog(email : String, pw : String)
        fun tryToResetPassword(email: String)
    }

    interface ViewLogIn{
        fun onLogInSuccess(user: FirebaseUser)
        fun onLogInFail(err: Int)
        fun errorDataHandler(err : Int) : Boolean

    }

    interface onLogInPresenterListener{
        fun onLogInSuccess(user: FirebaseUser)
        fun onLogInFail(errorCode: String)
    }
    interface onLogInModelListener{
        fun onLogInSuccess(user: FirebaseUser)
        fun onLogInFail(exception: Exception)
    }
}