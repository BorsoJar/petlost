package it.pdm.petlost.interfaces

import android.graphics.Bitmap
import com.google.android.libraries.places.api.model.Place
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.MyLatLng
import it.pdm.petlost.model.data.Report
import java.util.*

interface NewReportInterface {
    interface ModelNewReport
    {
        fun addNewReportRequest(report: Report, uuid: UUID)
        fun addNewImage(image: Bitmap, uuid: UUID) {
        }
    }

    interface PresenterNewReport
    {
        fun tryToAddNewReport(report: Report, uuid: UUID)
        fun checkReportInfo(
            data: Calendar,
            desc: String,
            animal: Animale?,
            place: Place?,
            latlng: MyLatLng?
        ) : Int
        fun onCreationReportSuccess()
        fun onCreationReportFailure()
        fun tryToAddNewImage(image: Bitmap, uuid: UUID)
    }

    interface ViewNewReport
    {
        fun init()
        fun onSuccess()
        fun onFailure()
    }

    interface OnNewReportCompleteListener
    {
        fun onCreationReportSuccess()
        fun onCreationReportFailure()
        fun uploadImageSuccess()
        fun uploadImageFailed()
    }
}