package it.pdm.petlost.interfaces

import android.content.Context

interface SettingsInterface {


    enum class ActionCode{
        BECOME_ADMIN
    }
    enum class ErrorCode{
        ALREADY_ADMIN,
        OPERATION_CANCELED,
        ADMIN_REQUEST_SUCCESS,
        ADMIN_REQUEST_FAIL
    }

    interface Model: onCompleteAction, GetUserInformation.Model{
        fun sendAdminRequest(request: String, context: Context): ErrorCode
        fun notificationsActivation(
            context: Context,
            enabled: Boolean
        )

    }

    interface Presenter: onCompleteAction, GetUserInformation.Presenter{
        fun tryToSendAdminRequest(request: String, context: Context): ErrorCode
        fun notificationsActivationRequest(
            context: Context,
            enabled: Boolean
        )
    }

    interface View: onCompleteAction{
        fun initView()

    }

    interface onCompleteAction{
//        fun settingsOnSuccess(requestCode: ActionCode)
//        fun settingsOnFailure(requestCode: ActionCode, errorCode: ErrorCode)
    }
}