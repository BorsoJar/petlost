package it.pdm.petlost.interfaces

import android.graphics.Bitmap
import it.pdm.petlost.model.data.CommentListItem

interface CommentsInterface {

    interface ViewInterface{
        fun errorDataHandler(error: Int) : Boolean
        fun reloadComments(commentsReport: List<CommentListItem>?)
    }

    interface PresenterInterface{
        fun getCommentsReport(repId : String) : List<CommentListItem>?
        fun tryToWriteComment(text : String, reportId : String)
        fun checkInfo(msg : String?) : Int
        fun reloadInfo(commentsReport: List<CommentListItem>?)
        fun deletePresenter()
        fun removeListener(reportId: String)
        fun assignListener(reportId: String)
        fun commentError(commentChangeErr: Int)
        fun tryToWriteCommentWithImage(reportId: String, text: String?, img: Bitmap?)
    }

    interface ModelInterface{
        fun getCommentsReport(repId : String) : List<CommentListItem>?
        fun writeComment(text : String, reportId : String)
        fun deleteCommentPresenter()
        fun assignListener(reportId: String)
        fun removeListener(reportId: String)
        fun writeCommentWithImage(reportId: String, text: String, img : Bitmap)

    }

}