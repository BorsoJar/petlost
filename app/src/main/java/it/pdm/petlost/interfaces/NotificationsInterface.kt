package it.pdm.petlost.interfaces

import android.content.Context
import it.pdm.petlost.model.data.Report

interface NotificationsInterface {
    enum class Error{
        NONE,
        USER_DISCONNECTED,
        ERROR_DATABASE_READING,
        ERROR_INIT_NOTIFICATIONS_SERVICE,
        REPORT_IMAGE_DEOSNT_EXIST,
        FAILED_DOWNLOAD_REPORT_IMAGE,
        NOTIFICATIONS_DISABLED,
        STATE_UPGRADE
    }

    enum class Action{
        GETTING_LAST_ADDED_REPORT,
        ON_NEW_REPORT,
        GET_REPORT_IMAGE_CALLBY_SERVICE,
        GET_REPORT_IMAGE_CALLBY_PRESENTER,
        SHOULD_NOTIFICATIONS_BE_ACTIVATED,
        REPORT_STATE_UPGRADE_TO_PROGRESS,
        REPORT_STATE_UPGRADE_TO_CLOSED
    }

    interface Service : NewReportListener
    {
        fun initializeNotificationManager()
        fun showNotification(error: Error, newReport: Pair<String, Report>?, stateUpgrade: Action?)
    }

    interface Model : NewReportListener
    {
        fun getLastAddedReportRequest()
        fun downloadReportImageForNotificationRequest(reportID: String, action: Action)
        fun isAdminCheckRequestForNotification(broadcastReceiver: NewReportListener)
        fun isNotificationActivated(context: Context): Boolean
        fun disableListenerForNewReportNotificationsRequest()
    }


    interface NewReportListener
    {
        fun onSuccess(action: Action, obj: Any?, broadcastReceiver: NewReportListener?)
        fun onFail(error: Error, extraInfo: Action?, broadcastReceiver: NewReportListener?)
    }

    interface presenter : NewReportListener
    {
        fun tryToGetReportImage(reportID: String)

    }

}