package it.pdm.petlost.interfaces

import android.graphics.Bitmap
import it.pdm.petlost.model.data.User

interface GetUserInformation {
    interface Model {
        fun getUserReference(): User
        fun getUserPhoto(): Bitmap?
    }

    interface Presenter {
        fun getName(): String
        fun getSurname(): String
        fun getPhone(): String
        fun getPhoto(): Bitmap
        fun getEmail(): String
        fun isAdmin(): Boolean
        fun hasPhoto(): Boolean
    }
}