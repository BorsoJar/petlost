package it.pdm.petlost.interfaces

import it.pdm.petlost.model.data.UserAuth

interface SignUpInterface {
    interface PresenterSignUp {
        fun tryToSign(name: String, surname: String, email: String, pw: String, emailconf: String, passconf: String)
    }

    interface ModelSignUp {
        fun SignUpRequest(name: String, surname: String, email: String, pw: String)
    }

    interface ViewSignUp {
        fun errorDataHandler(error: Int) : Boolean
        fun onSignUpSuccess()
        fun onSignUpFailure(risultato: String)
    }

    interface onRegistrationModelListener {
        fun onSignUpSuccess(user: UserAuth)
        fun onSignUpFailure(exception: Exception?)
        fun onDataSavingSuccess()
        fun onDataSavingFailure()
    }

    interface onRegistrationPresenterListener {
        fun onSuccess()
        fun onFailure(messaggio: String)
    }

}