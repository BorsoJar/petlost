package it.pdm.petlost.interfaces


import it.pdm.petlost.model.data.ReportListItem

interface MyReportInterface {

        interface MyModelReport{
            fun getReportsDataForList(id: Boolean) : List<ReportListItem>
            fun getCurrentUser(): String
            fun deleteReport(reportId : String)
            fun deleteMyRepListPresenter()
        }

        interface MyRepPresenter {
            fun getReportsData(id: Boolean): List<ReportListItem>
            fun getCurrentUser(): String
            fun deleteMyRepListPresenter()
            fun updateData(reportsDataForList: List<ReportListItem>)
        }

        interface MyViewReport{
            fun onDeleteSuccess()
            fun onDeleteFailed()
            fun updateView(reportsDataForList: List<ReportListItem>)
        }

    }
