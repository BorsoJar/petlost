package it.pdm.petlost.interfaces

import android.content.Context
import android.graphics.Bitmap

interface MainActivityInterface {

    enum class ActionCode{
        LOGOUT,
        USER_INFO,
        USER_PHOTO
    }

    interface MainActivityModel : OnCompleteUserInfoRequest, GetUserInformation.Model{
        fun requestUserInfo()
        fun requestLogout(context: Context)
        fun deleteMainActivityPresenter()
    }

    interface MainActivityPresenter : OnCompleteUserInfoRequest, GetUserInformation.Presenter{
        fun tryToGetUserInfo()
        fun tryToLogout(context: Context)
        fun deletePresenter()

    }

    interface MainActivityView : OnCompleteUserInfoRequest{

        fun initView()
        fun setUserInfoOnDrawer(name: String, surname: String, email: String)
        fun setUserPhotoOnDrawer(photo: Bitmap)
        fun returnToLoginActivity()
    }


    interface OnCompleteUserInfoRequest{
        fun onSuccess(requestCode: ActionCode, value: Any?) //L'argomento lo passo dal db al model solo per i dati dell'utente....negli altri contesti non serve quindi passo un valore null
        fun onFailure(requestCode: ActionCode)
    }
}