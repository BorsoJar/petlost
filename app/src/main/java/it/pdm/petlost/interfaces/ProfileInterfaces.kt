package it.pdm.petlost.interfaces

import android.graphics.Bitmap
import it.pdm.petlost.model.data.User

interface ProfileInterfaces {

    enum class ActionCode{
        NAME_UPDATE,
        SURNAME_UPDATE,
        PHONE_UPDATE,
        PASSWORD_UPDATE,
        PHOTO_UPDATE
    }
    enum class ErrorCode{
        AUTH_ERROR,
        GENERIC_ERROR
    }


    interface Model : onCompleteAction, GetUserInformation.Model {
        fun nameChangeRequest(newName: String, resultHandler: onCompleteAction)
        fun surnameChangeRequest(newSurname: String, resultHandler: onCompleteAction)
        fun photoChangeRequest(newPhoto: Bitmap, resultHandler: onCompleteAction)
        fun phoneChangeRequest(newPhone: String, resultHandler: onCompleteAction)
        fun passwordChangeRequest(hashOldPsw: String, hashNewPsw: String, resultHandler: onCompleteAction)
    }

    interface Presenter: onCompleteAction, GetUserInformation.Presenter {
        fun whichDataHasChanged(newName: String, newSurname: String, newPhone: String, newPhoto: Bitmap) : BooleanArray
        fun tryToChangeName(newName: String)
        fun tryToChangeSurname(newSurname: String)
        fun tryToChangePhone(newPhone: String)
        fun tryToChangePhoto(newPhoto: Bitmap)
        fun tryToChangePassword(oldPsw: String, newPsw: String)

        fun checkPassword(oldPsw: String, newPsw: String, newPswConfirmation: String) : Int

    }

    interface View: onCompleteAction{
        fun init()
        fun handleErrorPasswordChange(errorCode: Int)

    }

    interface onCompleteAction{
        fun onSuccess(requestCode: ActionCode)
        fun onFailure(requestCode: ActionCode, errorMessage: ErrorCode)
    }


}