package it.pdm.petlost.model.interfacesModel

import android.graphics.Bitmap
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseException
import it.pdm.petlost.interfaces.*
import it.pdm.petlost.model.data.Report
import it.pdm.petlost.model.data.Stato
import it.pdm.petlost.model.data.User
import it.pdm.petlost.presenter.CommentsPresenter
import java.lang.Exception
import java.util.*
import kotlin.collections.HashMap

interface PetLostManagerInterface {

    fun setMapsPresenter(mapsPresenter: MapsReportInterface.PresenterReport) : MapsReportInterface.ModelReport
    fun setNewReportPresenter(newReportPresenter: NewReportInterface.PresenterNewReport): NewReportInterface.ModelNewReport
    fun setProfilePresenter(profilePresenter: ProfileInterfaces.Presenter): ProfileInterfaces.Model

    fun setNotificationsService(notificationsService: NotificationsInterface.Service)

    fun setMainActivityPresenter(mainActivityPresenter: MainActivityInterface.MainActivityPresenter): MainActivityInterface.MainActivityModel
    fun setReportListPresenter(reportListPresenter: ReportListInterface.PresenterReportList?): ReportListInterface.ModelReport
    fun setMyRepListPresenter(myRepListPresenter: MyReportInterface.MyRepPresenter?): MyReportInterface.MyModelReport
    fun setReportItemPresenter(reportItemPresenter: ReportListInterface.PresenterReportItem): ReportListInterface.ModelReport
    fun setSettingsPresenter(settingsPresenter: SettingsInterface.Presenter): SettingsInterface.Model
    fun setCommentsPresenter(commentsPresenter: CommentsPresenter): CommentsInterface.ModelInterface
    fun onDataChange(reportId: String?)


    interface Listener {
        fun onReportSuccess(data : DataSnapshot)
        fun onFailed(e : Exception)
        fun onImageSuccess(id : String, byte : ByteArray)
        fun onDeleteReport(codeR : Int, codeC : Int)
        fun onWriteComplete(reportId: String)
        fun onWriteFailed(err : String)
        fun uploadCommentWithImageSuccess()
        fun uploadCommentWithImageFailed(exception: Exception?)
        fun onCommentChangeFailed(toException: DatabaseException)
        fun onCommentChange(dataSnapshot: DataSnapshot)
        fun onCommentDeletedSuccess()
        fun onCommentDeletedFailed(it: Exception)
        fun onCommentImageDownloaded(image: Bitmap?, imgId: String, repId: String)
    }


}
