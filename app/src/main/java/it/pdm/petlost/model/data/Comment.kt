package it.pdm.petlost.model.data

import android.icu.text.MessageFormat
import com.google.android.gms.maps.model.LatLng

open class Comment{

    var authorID: String
    var date: Long
    var description: String
    var authornick: String
    var photoID: String?
    var reportId : String
    var commId : String


    constructor()
    {
        this.authorID = ""
        this.date = -1
        this.description = ""
        this.authornick = ""
        this.photoID = ""
        this.reportId = ""
        this.commId = ""
    }

    constructor(authorID: String, date: Long, description: String)
    {
        this.authorID = authorID
        this.date = date
        this.description = description
        this.authornick = ""
        this.photoID = ""
        this.reportId = ""
        this.commId = ""
    }
    constructor(authorID: String, date: Long, description: String, authornick : String, photoID: String)
    :this(authorID, date, description)
    {
        this.photoID = photoID
        this.authornick = authornick
    }

    constructor(authorID: String, date: Long, description: String, authornick: String, reportId : String,photoID: String?, commID : String )
            :this(authorID, date, description)
    {
        this.reportId = reportId
        this.commId = commID
        this.authornick = authornick
        this.photoID = photoID
    }


}