package it.pdm.petlost.model

import com.google.firebase.auth.FirebaseAuthEmailException
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseUser
import it.pdm.petlost.model.db.FirebaseController
import it.pdm.petlost.model.util.PasswordUtil
import it.pdm.petlost.interfaces.LogInInterface

class LogInModel : LogInInterface.ModelLogIn, LogInInterface.onLogInModelListener {
    private val firebaseCtrl = FirebaseController
    private var logInPresenterHandler: LogInInterface.onLogInPresenterListener

    constructor(logInHandler: LogInInterface.onLogInPresenterListener)
    {
        this.logInPresenterHandler = logInHandler
    }

    override fun logInRequest(email: String, pw: String) {
        return firebaseCtrl.logIn(email, pw, this)
    }

    override fun onLogInSuccess(user: FirebaseUser) {
        logInPresenterHandler.onLogInSuccess(user)
    }

    override fun onLogInFail(exception: Exception){
        val errorCode: String = when(exception) {
            is FirebaseAuthEmailException -> "EMAIL_NOT_VERIFIED"
            is FirebaseAuthException -> exception.errorCode
            else -> "Errore genererico nel login"
        }
        logInPresenterHandler.onLogInFail(errorCode)

    }

    override fun sendMailConfirmationAgain()
    {
        firebaseCtrl.sendEmailVerify()
    }

    override fun resetPasswordRequest(email: String) {
        firebaseCtrl.resetPassword(email)
    }

}