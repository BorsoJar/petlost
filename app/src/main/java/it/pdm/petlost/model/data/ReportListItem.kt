package it.pdm.petlost.model.data

import android.content.Context
import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable
import java.text.SimpleDateFormat
import java.util.*

data class ReportListItem(val reportId: String, val authorId: String, var place: String, val data: Long, val desc: String, val anType: String, val state: String, var img: Bitmap?)  {

    fun retriveDate(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("dd.MM.yyyy HH:mm")
        return format.format(date)
    }


}