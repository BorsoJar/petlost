package it.pdm.petlost.model.db

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthEmailException
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import it.pdm.petlost.interfaces.*
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.data.*
import it.pdm.petlost.model.interfacesModel.DatabaseInterface
import it.pdm.petlost.model.interfacesModel.PetLostManagerInterface
import java.io.ByteArrayOutputStream
import java.util.*


object FirebaseController : DatabaseInterface {

    private val firebaseStorage = Firebase.storage
    private val firebaseAuth: FirebaseAuth = Firebase.auth
    private val firebaseDB = Firebase.database
    private var dbCommentListener: ValueEventListener? = null
    private lateinit var dbReportListener: ValueEventListener
    private var petListener: PetLostManagerInterface.Listener = PetLostManager
    private lateinit var childEventListener_notificationService: ChildEventListener

    private val DELETE_REPORT_SUCCESS = 0
    private val DELETE_REPORT_FAILED = -1
    private val DELETE_COMMENT_SUCCESS = 1
    private val DELETE_COMMENT_FAILED = -2

    /*-------------------------------   SETTER METHOD     -------------------------------*/

    override fun setReportCommentsListener(reportId: String) {
        firebaseDB.reference.child("comment").child(reportId)
            .addValueEventListener(dbCommentListener!!)
    }

    override fun deleteReportCommentsListener(reportId: String) {
        firebaseDB.reference.child("comment").child(reportId)
            .removeEventListener(dbCommentListener!!)
    }



    /*-------------------------------   LOG && SIGN METHOD     -------------------------------*/

    override fun logIn(email: String, pwSHA5: String, logInModelHandler: LogInInterface.onLogInModelListener) {
        firebaseAuth.signInWithEmailAndPassword(email, pwSHA5).addOnCompleteListener { task ->
            if (task.isSuccessful)
                if (!firebaseAuth.currentUser!!.isEmailVerified) {
                    Log.i(
                        "FirebaseController",
                        "User loggato correttamente MA email non verificata"
                    )
                    Firebase.auth.signOut()
                    logInModelHandler.onLogInFail(
                        FirebaseAuthEmailException(
                            "Email isn't verified",
                            "verify you email!"
                        )
                    )
                } else {
                    Log.i("FirebaseController", "User loggato correttamente")

                    logInModelHandler.onLogInSuccess(firebaseAuth.currentUser!!)
                }
            else {
                Log.e(
                    "FirebaseController",
                    "Tentativo di login fallito: ${task.exception.toString()}"
                )
                logInModelHandler.onLogInFail(task.exception!!)
            }
        }
//        updateMapsData(petListener)
    }

    override fun logOut(listener: MainActivityInterface.MainActivityModel) {
        firebaseAuth.signOut()
        firebaseDB.goOffline()
        listener.onSuccess(MainActivityInterface.ActionCode.LOGOUT, null)
    }

    override fun signUp(name: String, surname: String, email: String, pwSHA5: String, signUpHandler: SignUpInterface.onRegistrationModelListener) {
        val user = UserAuth(email = email, psw = pwSHA5)

        firebaseAuth.createUserWithEmailAndPassword(email, pwSHA5).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                user.name = name
                user.surname = surname
                user.uid = firebaseAuth.currentUser!!.uid

                signUpHandler.onSignUpSuccess(user)

            } else {
                Log.e(
                    "FirebaseController",
                    "Tentativo di registrazione fallito: ${task.exception.toString()}"
                )
                signUpHandler.onSignUpFailure(task.exception)
            }
        }
    }

    override fun sendEmailVerify() {
        firebaseAuth.currentUser!!.sendEmailVerification().addOnCompleteListener { task ->
            if (task.isSuccessful)
                Log.i("FirebaseController", "email confirmation sent")
            else
                Log.e(
                    "FirebaseController",
                    "error sending email confirmation: ${task.exception.toString()}"
                )
        }

    }



    /*-------------------------------   USER METHOD     -------------------------------*/

    override fun addUser(user: UserAuth, signUpModelHandler: SignUpInterface.onRegistrationModelListener) {
        firebaseDB.getReference("users").child(user.uid).setValue(user)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.i("FirebaseController", "User ${user.uid} aggiunto correttamente")
                    signUpModelHandler.onDataSavingSuccess()
                } else {
                    Log.e(
                        "FirebaseController",
                        "Tentativo di scrittura utente ${user.uid} fallito: ${task.exception.toString()}"
                    )
                    signUpModelHandler.onDataSavingFailure()
                }
            }
    }

    override fun removeUser() {
        firebaseAuth.currentUser!!.delete()
    }

    override fun getUserInfo(listener: MainActivityInterface.MainActivityModel) {
        firebaseDB.getReference("users").child(firebaseAuth.currentUser!!.uid).get()
            .addOnSuccessListener {
                listener.onSuccess(
                    MainActivityInterface.ActionCode.USER_INFO,
                    it.getValue(User::class.java) as User
                )
            }.addOnFailureListener {
                listener.onFailure(MainActivityInterface.ActionCode.USER_INFO)
            }

    }

    override fun getUserId(): String {
        return firebaseAuth.currentUser!!.uid
    }

    override fun uploadUserProfilePhoto(image: Bitmap, listener: ProfileInterfaces.onCompleteAction) {
        val storage = firebaseStorage.getReference("userProfilePhoto")
        val bytearros = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, bytearros)
        val data = bytearros.toByteArray()
        if (firebaseAuth.currentUser != null) {
            storage.child(firebaseAuth.currentUser.uid).putBytes(data)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful)
                        listener.onSuccess(ProfileInterfaces.ActionCode.PHOTO_UPDATE)
                    else
                        listener.onFailure(
                            ProfileInterfaces.ActionCode.PHOTO_UPDATE,
                            ProfileInterfaces.ErrorCode.GENERIC_ERROR
                        )
                }
        }
    }

    override fun downloadUserProfilePhoto(id: String, listener: MainActivityInterface.MainActivityModel) {
        val ONE_MEGABYTE: Long = 1024 * 1024 * 1024 //todo gestire meglio la size del buffer
        val storage = firebaseStorage.reference
        storage.child("userProfilePhoto").child(id).getBytes(ONE_MEGABYTE).addOnSuccessListener {
            listener.onSuccess(MainActivityInterface.ActionCode.USER_PHOTO, it)
        }.addOnFailureListener {
            listener.onFailure(MainActivityInterface.ActionCode.USER_PHOTO)
        }
    }




    /*-------------------------------   REPORT METHOD     -------------------------------*/

    override fun createReport(report: Report, handlerModel: NewReportInterface.OnNewReportCompleteListener, uuid: UUID) {
        firebaseDB.getReference("report").child(uuid.toString()).setValue(report)
            .addOnCompleteListener { task ->
                if (task.isSuccessful)
                    handlerModel.onCreationReportSuccess()
                else {
                    handlerModel.onCreationReportFailure()
                    Log.e(
                        "FirebaseController",
                        "error create report: ${task.exception.toString()}"
                    )
                }
            }

    }

    override fun deleteReport(reportId: String, commentsID: List<String>?, petlistener: PetLostManagerInterface.Listener) {
        firebaseDB.getReference("report").child((reportId)).removeValue().addOnSuccessListener {
            if (commentsID != null && commentsID.isNotEmpty()) {
                for (id in commentsID)
                    firebaseDB.getReference("comment").child(id).removeValue()
            }
            petlistener.onDeleteReport(DELETE_REPORT_SUCCESS, DELETE_COMMENT_SUCCESS)
        }.addOnFailureListener {
            petlistener.onDeleteReport(DELETE_REPORT_FAILED, DELETE_COMMENT_FAILED)
            Log.e("DELETE", it.toString())
        }
    }

    override fun updateReportState(reportId: String, state: Stato) {
        val fbtmp = firebaseDB.getReference("report").child(reportId)
       fbtmp.get().addOnSuccessListener {
           val tmpreport = it.getValue(Report::class.java)
           tmpreport!!.reportState = state
           fbtmp.setValue(tmpreport)
           PetLostManager.onDataChange(reportId)
       }
    }



    /*-------------------------------   IMAGE METHOD     -------------------------------*/

    override fun downloadImage(idimg: String, listener: PetLostManagerInterface.Listener) {
        val ONE_MEGABYTE: Long = 1024 * 1024 * 1024
        val storage = firebaseStorage.reference
        storage.child("images").child(idimg).getBytes(ONE_MEGABYTE).addOnSuccessListener {
            listener.onImageSuccess(idimg, it)
        }.addOnFailureListener {
           listener.onFailed(it)
        }
    }

    override fun uploadImage(uuid: UUID, image: Bitmap, listener: NewReportInterface.OnNewReportCompleteListener) {
        val storage = firebaseStorage.getReference("images")
        val bytearros = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, bytearros)
        val data = bytearros.toByteArray()
        storage.child(uuid.toString()).putBytes(data).addOnCompleteListener { task ->
            if (task.isSuccessful)
                listener.uploadImageSuccess()
            else
                listener.uploadImageFailed()
        }

    }




    /*-------------------------------   MAPS METHOD     -------------------------------*/

    override fun updateMapsData(petListener: PetLostManagerInterface.Listener) {
        if (firebaseAuth.currentUser != null) {
            dbReportListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    FirebaseController.petListener.onReportSuccess(dataSnapshot)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    FirebaseController.petListener.onFailed(databaseError.toException())
                }
            }
            firebaseDB.reference.child("report").addValueEventListener(dbReportListener)

        }
    }


    /*-------------------------------   COMMENT METHOD     -------------------------------*/

    override fun updateCommentsData(petListener: PetLostManagerInterface.Listener,repId: String) {
        dbCommentListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                FirebaseController.petListener.onCommentChange(dataSnapshot)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                FirebaseController.petListener.onCommentChangeFailed(databaseError.toException())
            }
        }
        firebaseDB.reference.child("comment").child(repId).addValueEventListener(dbCommentListener!!)
    }

    override fun deleteListener(reportId: String) {
        firebaseDB.reference.child("comment").child(reportId).removeEventListener(dbCommentListener!!)
    }

    override fun deleteComment(reportId: String, petListener: PetLostManagerInterface.Listener) {
        firebaseDB.reference.child("comment").child(reportId).removeValue().addOnSuccessListener {
            petListener.onCommentDeletedSuccess()
        }.addOnFailureListener {
            petListener.onCommentDeletedFailed(it)
        }
    }

    override fun writeComment(comment: MutableMap<String, MutableMap<String, Comment>>, reportId: String, petListener: PetLostManagerInterface.Listener) {
        firebaseDB.getReference("comment").updateChildren(comment as Map<String, List<Comment>>)
            .addOnSuccessListener {
                petListener.onWriteComplete(reportId)
            }.addOnFailureListener {
                petListener.onWriteFailed(it.toString())
            }
    }

    override fun writeCommentWithImage(comment: MutableMap<String, MutableMap<String, Comment>>,reportId: String, uuid: String,img: Bitmap, petListener: PetLostManagerInterface.Listener) {
        val storage = firebaseStorage.getReference("images")
        val bytearros = ByteArrayOutputStream()
        img.compress(Bitmap.CompressFormat.JPEG, 100, bytearros)
        val data = bytearros.toByteArray()
        storage.child(uuid).putBytes(data).addOnCompleteListener { task ->
            if (task.isSuccessful)
                petListener.uploadCommentWithImageSuccess()
            else
                petListener.uploadCommentWithImageFailed(task.exception)
            bytearros.close()
        }
        writeComment(comment, reportId, petListener)
    }

    override fun downloadCommentImage(imgId: String, petListener: PetLostManagerInterface.Listener, repId: String){
        val ONE_MEGABYTE: Long = 1024 * 1024 * 1024
        val storage = firebaseStorage.reference
        storage.child("images").child(imgId).getBytes(ONE_MEGABYTE).addOnSuccessListener {
            petListener.onCommentImageDownloaded(
                image = BitmapFactory.decodeByteArray(it, 0, it.size),
                imgId = imgId,
                repId
            )
        }.addOnFailureListener {
            Log.e("FirebaseCtrl", it.message.toString())
        }
    }

    override fun onNewReport(listener: NotificationsInterface.NewReportListener, lastReport_creationDate: Long) {
        childEventListener_notificationService = firebaseDB.getReference("report").addChildEventListener(  object :ChildEventListener{
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val key = snapshot.key
                val tmp = snapshot.getValue(Report::class.java)
                if(tmp!!.creationDate > lastReport_creationDate && firebaseAuth.currentUser.uid != tmp!!.authorID) {
                    listener.onSuccess(
                        NotificationsInterface.Action.ON_NEW_REPORT,
                        Pair(key, tmp),
                        null
                    )
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val key = snapshot.key
                val report = snapshot.getValue(Report::class.java)
                if(report!!.authorID == firebaseAuth.currentUser.uid)
                    when(report!!.reportState)
                    {
                        Stato.PRESA_IN_CARICO -> listener.onSuccess(NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_PROGRESS, Pair(key, report), null)
                        Stato.PRESA_IN_CARICO -> listener.onSuccess(NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_CLOSED, Pair(key, report), null)
                        else -> listener.onFail(NotificationsInterface.Error.STATE_UPGRADE, null, null)
                    }
                else
                    listener.onFail(NotificationsInterface.Error.STATE_UPGRADE, null, null)


            }

            override fun onChildRemoved(snapshot: DataSnapshot) {}

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}

            override fun onCancelled(error: DatabaseError) {}
        })

    }

    override fun disableListenerForNewReportNotifications() {
        firebaseDB.getReference("report").removeEventListener(childEventListener_notificationService)
    }

    override fun getLastAddedReport(listener: NotificationsInterface.NewReportListener) {
        if(firebaseAuth.currentUser != null) {
            firebaseDB.getReference("report").orderByChild("creationDate").limitToFirst(1)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val tmp = snapshot.getValue(Report::class.java)
                        listener.onSuccess(
                            NotificationsInterface.Action.GETTING_LAST_ADDED_REPORT,
                            tmp!!.creationDate,
                            null
                        )
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.e("FirebaseController","Non sono riuscito ad ottenere l'ultima report inserita")
                        listener.onFail(
                            NotificationsInterface.Error.ERROR_INIT_NOTIFICATIONS_SERVICE,
                            null,
                            null
                        )
                    }
                })
        }else
            listener.onFail(NotificationsInterface.Error.USER_DISCONNECTED, null, null)

    }

    override fun downloadReportImageForNotification(
        imageID: String,
        listener: NotificationsInterface.NewReportListener,
        callBy: NotificationsInterface.Action
    ) {
        val ONE_MEGABYTE: Long = 1024 * 1024 * 1024
        val storage = firebaseStorage.reference
        storage.child("images").child(imageID).getBytes(ONE_MEGABYTE).addOnSuccessListener {
            listener.onSuccess(callBy, BitmapFactory.decodeByteArray(it, 0, it.size), null)
        }.addOnFailureListener {
            listener.onFail(NotificationsInterface.Error.FAILED_DOWNLOAD_REPORT_IMAGE, callBy, null)
        }
    }

    override fun queryCurrentUserToCheckIfHeIsAdmin(
        listener: NotificationsInterface.NewReportListener,
        broadcastReceiver: NotificationsInterface.NewReportListener
    ) {
        if(firebaseAuth.currentUser != null)
        {
            firebaseDB.getReference("users").child(firebaseAuth.currentUser.uid).get().addOnCompleteListener { task->
                if(task.isSuccessful)
                {
                    try {
                        val dataSnapshot = task.result
                        val user = dataSnapshot.getValue(User::class.java)
                        listener.onSuccess(NotificationsInterface.Action.SHOULD_NOTIFICATIONS_BE_ACTIVATED, user!!.isAdmin as Boolean, broadcastReceiver)
                    }
                    catch (e: Exception)
                    {
                        Log.e("firebaseController", e.message.toString())
                        listener.onFail(NotificationsInterface.Error.ERROR_DATABASE_READING, NotificationsInterface.Action.SHOULD_NOTIFICATIONS_BE_ACTIVATED, broadcastReceiver)
                    }

                }
                else
                    listener.onFail(NotificationsInterface.Error.ERROR_DATABASE_READING, NotificationsInterface.Action.SHOULD_NOTIFICATIONS_BE_ACTIVATED, broadcastReceiver)
            }
        }
        else
            listener.onFail(NotificationsInterface.Error.USER_DISCONNECTED, NotificationsInterface.Action.SHOULD_NOTIFICATIONS_BE_ACTIVATED, broadcastReceiver)
    }

/*-------------------------------   PROFILE METHOD     -------------------------------*/

    override fun resetPassword(email: String) {
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if (task.isSuccessful)
                Log.d("FirebaseController", "Reset password email sent corrretcly.")
            else
                Log.e("FirebaseController", "Reset password email error: ${task.exception}")
        }
    }

    override fun changePassword(email: String, oldPassword: String, newPassword: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction) {
        if (firebaseAuth.currentUser != null) {
            val credential = EmailAuthProvider.getCredential(email, oldPassword)
            firebaseAuth.currentUser.reauthenticate(credential).addOnCompleteListener { task ->
                if (task.isSuccessful)
                    firebaseAuth.currentUser.updatePassword(newPassword)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful)
                                onCompleteActionHandler.onSuccess(ProfileInterfaces.ActionCode.PASSWORD_UPDATE)
                            else {
                                Log.e(
                                    "ForebaseController",
                                    "Errore nella reautenticazione per il cambio password: ${task.exception.toString()}"
                                )
                                onCompleteActionHandler.onFailure(
                                    ProfileInterfaces.ActionCode.PASSWORD_UPDATE,
                                    ProfileInterfaces.ErrorCode.GENERIC_ERROR
                                )
                            }
                        }
                else {
                    Log.e(
                        "ForebaseController",
                        "Errore nella reautenticazione per il cambio password: ${task.exception.toString()}"
                    )
                    onCompleteActionHandler.onFailure(
                        ProfileInterfaces.ActionCode.PASSWORD_UPDATE,
                        ProfileInterfaces.ErrorCode.AUTH_ERROR
                    )
                }
            }
        }
    }

    override fun updataUserName(newName: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction) {
        val currentUser = firebaseAuth.currentUser
        firebaseDB.getReference("users").child(currentUser.uid).child("name").setValue(newName)
            .addOnCompleteListener { task ->
                if (task.isSuccessful)
                    onCompleteActionHandler.onSuccess(ProfileInterfaces.ActionCode.NAME_UPDATE)
                else
                    onCompleteActionHandler.onFailure(
                        ProfileInterfaces.ActionCode.NAME_UPDATE,
                        ProfileInterfaces.ErrorCode.GENERIC_ERROR
                    )
            }
    }

    override fun updataUserSurname(newSurname: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction) {
        val currentUser = firebaseAuth.currentUser
        firebaseDB.getReference("users").child(currentUser.uid).child("surname")
            .setValue(newSurname).addOnCompleteListener { task ->
                if (task.isSuccessful)
                    onCompleteActionHandler.onSuccess(ProfileInterfaces.ActionCode.SURNAME_UPDATE)
                else
                    onCompleteActionHandler.onFailure(
                        ProfileInterfaces.ActionCode.SURNAME_UPDATE,
                        ProfileInterfaces.ErrorCode.GENERIC_ERROR
                    )
            }
    }

    override fun updataUserPhone(NewPhone: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction) {
        val currentUser = firebaseAuth.currentUser
        firebaseDB.getReference("users").child(currentUser.uid).child("phone").setValue(NewPhone)
            .addOnCompleteListener { task ->
                if (task.isSuccessful)
                    onCompleteActionHandler.onSuccess(ProfileInterfaces.ActionCode.PHONE_UPDATE)
                else
                    onCompleteActionHandler.onFailure(
                        ProfileInterfaces.ActionCode.PHONE_UPDATE,
                        ProfileInterfaces.ErrorCode.GENERIC_ERROR
                    )
            }
    }


    override fun openConnection() {
        firebaseDB.goOnline()
    }


}




