package it.pdm.petlost.model

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.NotificationsInterface


class StartNotificationsService : BroadcastReceiver(),  NotificationsInterface.NewReportListener {
    var context:Context? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context?, intent: Intent?) {

        this.context = context

//        SCOMMENTARE SOLO SE SI VUOLE DEBUGGARE, ALTRIMENI SI PERDONO 2 ORE A CAPIRE PERCHE NON VA UNA SEGA!
//        Questa istruzione serve per mettere in attesa il programma fino all'intercettazione del processo di debug di andoridstudio
//       android.os.Debug.waitForDebugger();

        PetLostManager.isAdminCheckRequestForNotification(this)

    }

    private fun startNotificationService()
    {
        Log.i("StartNotificService", "notifiche abilitate")
        val intent = Intent(context, NotificationsService::class.java)
        context?.startService(intent)
    }
    override fun onSuccess(
        action: NotificationsInterface.Action,
        obj: Any?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when(action)
        {
            NotificationsInterface.Action.SHOULD_NOTIFICATIONS_BE_ACTIVATED ->
            {
                saveIfUserIsAdminOnSavedPreferences(obj as Boolean)
                if(obj as Boolean || PetLostManager.isNotificationActivated(context!!) )
                    startNotificationService()
                else
                    onFail(NotificationsInterface.Error.NOTIFICATIONS_DISABLED, null, null)

            }
        }
    }

    override fun onFail(
        error: NotificationsInterface.Error,
        extraInfo: NotificationsInterface.Action?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        //questo metodo viene chiamato solo nel caso in cui l'utente non ne admin ne ha espresso la volonta di ricevere notifiche oppure se l'user non è loggato
        when(error)
        {
            NotificationsInterface.Error.USER_DISCONNECTED -> Log.e("StartNotificService", "Impossibile far partire service notifiche perchè l'user non è loggato")
            NotificationsInterface.Error.NOTIFICATIONS_DISABLED -> Log.e("StartNotificService", "Notifiche disabilitate")
        }
    }
    private fun saveIfUserIsAdminOnSavedPreferences(isAdmin: Boolean)
    {
        val prefs = context!!.getSharedPreferences(context!!.getString(R.string.sharedPrefences), Context.MODE_PRIVATE)
        prefs.edit().putBoolean(context!!.getString(R.string.notification_flag_isAdmin), isAdmin).apply()
    }
}