package it.pdm.petlost.model

import com.google.firebase.auth.FirebaseAuthException
import it.pdm.petlost.model.data.UserAuth
import it.pdm.petlost.model.db.FirebaseController
import it.pdm.petlost.model.util.PasswordUtil
import it.pdm.petlost.interfaces.SignUpInterface

class SignUpModel : SignUpInterface.ModelSignUp, SignUpInterface.onRegistrationModelListener {

    private val firebaseCtrl = FirebaseController
    private lateinit var signUpPresenterHandler: SignUpInterface.onRegistrationPresenterListener

    constructor(signUpHandler: SignUpInterface.onRegistrationPresenterListener)
    {
        this.signUpPresenterHandler = signUpHandler
    }

    override fun SignUpRequest(name: String, surname: String, email: String, pw: String) {
        firebaseCtrl.signUp(name, surname, email, pw, this)
    }

    override fun onSignUpSuccess(user: UserAuth) {
        firebaseCtrl.addUser(user, this)
    }

    override fun onSignUpFailure(exception: Exception?) {
        var errorCode: String = "Errore genererico nella signup"
        if(exception is FirebaseAuthException)
            errorCode = exception.errorCode
        signUpPresenterHandler.onFailure(errorCode)

    }

    override fun onDataSavingSuccess() {
        signUpPresenterHandler.onSuccess()
        firebaseCtrl.sendEmailVerify()
    }

    override fun onDataSavingFailure() {
        firebaseCtrl.removeUser()
    }


}