package it.pdm.petlost.model.data

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import it.pdm.petlost.R


class MyLatLng(var lat: Double, var lng: Double) :Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble()
    ) {
    }

    constructor() : this(0.0,0.0)
    {}

    fun toLatLng(): LatLng
    {
        return LatLng(lat, lng)
    }

    fun convertLatLngToAddress(ltng : MyLatLng, context : Context) : String{
        var list = listOf<Address>()
        val geodecoder = Geocoder(context)
                try {
                    list = geodecoder.getFromLocation(ltng.lat, ltng.lng, 1)
                }
                catch(e : Exception){
                    Log.e("MyLatLng", "Error while get list from geodecoder ${e.message}")
                }
        return if(list.isNotEmpty())
            list[0].getAddressLine(0)
        else
            context.getString(R.string.geodecoder_err)+ltng.toString()
    }

      fun stringToLatLng(s : String) : MyLatLng{
        val liststring = s.split(" ")
        var lat = 0.0
        var lon = 0.0
        for((count, c) in liststring.withIndex()){
            when(c){
                "lat" -> lat = liststring[count+1].toDouble()
                "lon" -> lon = liststring[count+1].toDouble()
            }
        }
        return MyLatLng(lat,lon)
    }

    override fun toString(): String {
        return "lat $lat , lon $lng"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(lat)
        parcel.writeDouble(lng)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyLatLng> {
        override fun createFromParcel(parcel: Parcel): MyLatLng {
            return MyLatLng(parcel)
        }

        override fun newArray(size: Int): Array<MyLatLng?> {
            return arrayOfNulls(size)
        }
    }

}
