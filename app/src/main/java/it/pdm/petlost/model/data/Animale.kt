package it.pdm.petlost.model.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class Animale : Parcelable{
    CANE,
    GATTO,
    CONIGLIO,
    CRICETO,
    TARTARUGA,
    PAPPAGALLO,
    CANARINO,
    SERPENTE,
    GALLINA,
    RAGNO
}