package it.pdm.petlost.model.data


data class ReportsMapsItem(val title: String, val site: MyLatLng, val repid: String)