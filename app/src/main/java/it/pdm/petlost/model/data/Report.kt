package it.pdm.petlost.model.data


class Report
{
    var authorID : String
    var reportState: Stato
    var animaleType: Animale
    var site : MyLatLng
    var date : Long
    var desc : String
    var photoID : String
    var creationDate: Long

    constructor()
    {
        this.reportState = Stato.APERTA
        this.animaleType = Animale.CANE
        this.site = MyLatLng(0.0,0.0)
        this.authorID = ""
        this.date = 0L
        this.desc = ""
        this.photoID = ""
        this.creationDate = System.currentTimeMillis()
    }

    constructor(authorID: String, site: MyLatLng, date: Long, description: String, reportState: Stato, animaleType: Animale)
            : this()
    {
        this.site = site
        this.reportState = reportState
        this.animaleType = animaleType
        this.authorID = authorID
        this.date = date
        this.desc = description
        this.photoID = ""
    }

    constructor(authorID: String, site: MyLatLng, date: Long, description: String, photoID: String, reportState: Stato, animaleType: Animale)
            : this(authorID, site, date, description, reportState,animaleType)
    {
       this.photoID = photoID
    }

    override fun toString(): String {
        return "${authorID}, ${reportState}, ${animaleType}, ${site}, ${date}, ${desc}, ${photoID}"
    }

}
