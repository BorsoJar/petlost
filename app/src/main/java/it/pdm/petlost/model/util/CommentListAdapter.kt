package it.pdm.petlost.model.util

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.Adapter
import it.pdm.petlost.model.data.CommentListItem
import kotlinx.android.synthetic.main.recycle_view_comment_item.view.*
import kotlinx.android.synthetic.main.recycle_view_comment_with_image.view.*


private const val COMMENT_NO_IMAGE = 0
private const val COMMENT_WITH_IMAGE = 1

class CommentListAdapter(
    val commList: List<CommentListItem>,
    val listener: Adapter.ItemClickInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    inner class CommentListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private var dateText: TextView
        private var nickText: TextView
        private var descText: TextView


        init {
            itemView.setOnClickListener(this)
            this.descText = itemView.recycle_comment_item_desc
            this.dateText = itemView.recycle_comment_item_date
            this.nickText = itemView.recycle_comment_item_nick
        }

        fun bind(comment: CommentListItem) {
            dateText.text = comment.retriveDate(comment.data)
            descText.text = comment.desc
            nickText.text = comment.usernick
        }

        override fun onClick(v: View?) {
            if (adapterPosition != RecyclerView.NO_POSITION)
                listener.onItemClick(adapterPosition)
        }

    }

    inner class CommentImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private var imgView: ImageView
        private var dateText: TextView
        private var nickText: TextView
        private var descText: TextView

        init {
            itemView.setOnClickListener(this)
            this.descText = itemView.rec_comment_item_desc
            this.dateText = itemView.rec_comment_item_date
            this.imgView = itemView.rec_comment_image_view
            this.nickText = itemView.rec_comment_item_nick
        }

        fun bind(comment: CommentListItem) {
            imgView.setImageBitmap(scaleToFitHeight(comment.img!!))
            dateText.text = comment.retriveDate(comment.data)
            descText.text = comment.desc
            nickText.text = comment.usernick
        }

        override fun onClick(v: View?) {
            if (adapterPosition != RecyclerView.NO_POSITION)
                listener.onItemClick(adapterPosition)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == COMMENT_WITH_IMAGE) {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_view_comment_with_image, parent, false)
            CommentImageViewHolder(itemView)
        } else {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_view_comment_item, parent, false)
            CommentListViewHolder(itemView)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currItem = commList[position]
        if (getItemViewType(position) == COMMENT_WITH_IMAGE) {
            if (currItem.img != null)
                (holder as CommentImageViewHolder).bind(currItem)

        } else
            (holder as CommentListViewHolder).bind(currItem)
    }

    override fun getItemViewType(position: Int): Int {
        return if (commList[position].img == null)
            COMMENT_NO_IMAGE
        else
            COMMENT_WITH_IMAGE
    }

    override fun getItemCount() = commList.size

    private fun scaleToFitHeight(b: Bitmap): Bitmap {

        val factor = 120 / b.height.toFloat()
        return Bitmap.createScaledBitmap(b, (b.width * factor).toInt(), 130, true)
    }
}



