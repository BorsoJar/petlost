package it.pdm.petlost.model.util

import java.security.MessageDigest

class PasswordUtil() {

    companion object {
        fun encrypt(data: String): String {
            val bytes = data.toByteArray()
            val md = MessageDigest.getInstance("SHA-256")
            val digest = md.digest(bytes)
            return digest.fold("", { str, it -> str + "%02x".format(it) })
        }
    }

}