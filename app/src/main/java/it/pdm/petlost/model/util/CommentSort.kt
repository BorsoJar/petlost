package it.pdm.petlost.model.util

import it.pdm.petlost.model.data.CommentListItem

class CommentSort {

    fun quickSort(arr: ArrayList<CommentListItem>, begin: Int, end: Int) : ArrayList<CommentListItem> {
        if (begin < end) {
            val partitionIndex: Int = partition(arr, begin, end)
            quickSort(arr, begin, partitionIndex - 1)
            quickSort(arr, partitionIndex + 1, end)
        }
        return arr
    }

    private fun partition(arr: ArrayList<CommentListItem>, begin: Int, end: Int): Int {
        val pivot = arr[end].data
        var i = begin - 1
        for (j in begin until end) {
            if (arr[j].data <= pivot) {
                i++
                val swapTemp = arr[i]
                arr[i] = arr[j]
                arr[j] = swapTemp
            }
        }
        val swapTemp = arr[i + 1]
        arr[i + 1] = arr[end]
        arr[end] = swapTemp
        return i + 1
    }

}