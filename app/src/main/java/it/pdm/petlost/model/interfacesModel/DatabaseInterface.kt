package it.pdm.petlost.model.interfacesModel

import android.graphics.Bitmap
import it.pdm.petlost.interfaces.*
import it.pdm.petlost.model.data.Comment
import it.pdm.petlost.model.data.UserAuth
import it.pdm.petlost.model.data.Report
import it.pdm.petlost.model.data.Stato
import java.util.*

interface DatabaseInterface {

    fun logIn(email: String, pwSHA5: String, logInModelHandler: LogInInterface.onLogInModelListener)
    fun logOut(listener: MainActivityInterface.MainActivityModel)
    fun signUp(name : String, surname : String, email: String, pwSHA5 : String, signUpHandler: SignUpInterface.onRegistrationModelListener)
    fun addUser(user: UserAuth, signUpModelHandler: SignUpInterface.onRegistrationModelListener)
    fun removeUser()
    fun sendEmailVerify()
    fun createReport(
        report: Report,
        handlerModel: NewReportInterface.OnNewReportCompleteListener,
        uuid: UUID
    )
    fun getUserInfo(listener: MainActivityInterface.MainActivityModel)
    fun downloadImage(idimg : String, listener : PetLostManagerInterface.Listener)
    fun uploadImage(uuid: UUID,image : Bitmap,listener : NewReportInterface.OnNewReportCompleteListener)
    fun updateMapsData(petListener : PetLostManagerInterface.Listener)
    fun getUserId() : String
    fun changePassword(email: String, oldPassword: String, newPassword: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction)
    fun updataUserName(newName: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction)
    fun updataUserSurname(newSurname: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction)
    fun updataUserPhone(NewPhone: String, onCompleteActionHandler: ProfileInterfaces.onCompleteAction)

    fun downloadUserProfilePhoto(id: String, listener : MainActivityInterface.MainActivityModel)
    fun uploadUserProfilePhoto(image : Bitmap,listener : ProfileInterfaces.onCompleteAction)

    fun resetPassword(email: String)
    fun deleteReport(reportId: String,commentsID : List<String>?, petlistener : PetLostManagerInterface.Listener)
    fun writeComment(comment: MutableMap<String, MutableMap<String, Comment>>, reportId: String, petListener: PetLostManagerInterface.Listener)
    fun writeCommentWithImage(comment: MutableMap<String, MutableMap<String, Comment>>, reportId: String, uuid: String, img: Bitmap, petListener: PetLostManagerInterface.Listener)
    fun deleteReportCommentsListener(reportId: String)
    fun setReportCommentsListener(reportId: String)
    fun updateCommentsData(petListener: PetLostManagerInterface.Listener,repId: String)
    fun deleteListener(reportId: String)
    fun deleteComment(reportId: String, petListener: PetLostManagerInterface.Listener)
    fun downloadCommentImage(
        imgId: String,
        petListener: PetLostManagerInterface.Listener,
        repId: String
    )

    fun onNewReport(
        listener: NotificationsInterface.NewReportListener,
        lastReport_creationDate: Long
    )
    fun disableListenerForNewReportNotifications()
    fun getLastAddedReport(listener: NotificationsInterface.NewReportListener)
    fun downloadReportImageForNotification(
        imageID: String,
        listener: NotificationsInterface.NewReportListener,
        callBy: NotificationsInterface.Action
    )
    fun queryCurrentUserToCheckIfHeIsAdmin(
        listener: NotificationsInterface.NewReportListener,
        broadcastReceiver: NotificationsInterface.NewReportListener
    )

    fun updateReportState(reportId: String, state: Stato)
    fun openConnection()

}