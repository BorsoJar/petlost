package it.pdm.petlost.model.data

import android.graphics.Bitmap
import java.text.SimpleDateFormat
import java.util.*

class CommentListItem{

    var commid : String
    var userId : String
    var usernick : String
    var data : Long
    var desc : String
    var img : Bitmap?

  constructor(){
      this.commid = ""
      this.userId = ""
      this.usernick = ""
      this.data = 0L
      this.desc = ""
      this.img = null
  }


    constructor(commid : String, userId : String, usernick : String, data : Long,  desc : String, img : Bitmap?)  {
        this.commid = commid
        this.userId = userId
        this.usernick = usernick
        this.data = data
        this.desc = desc
        this.img = img
    }

    fun retriveDate(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("dd.MM.yyyy HH:mm")
        return format.format(date)
    }
}
