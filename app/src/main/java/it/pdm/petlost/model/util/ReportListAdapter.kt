package it.pdm.petlost.model.util

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.Adapter
import it.pdm.petlost.model.data.ReportListItem
import it.pdm.petlost.model.data.Stato
import kotlinx.android.synthetic.main.recycle_view_report_item_no_image.view.*
import kotlinx.android.synthetic.main.recycle_view_report_item_with_image.view.*

class ReportListAdapter(
    private val reportList: List<ReportListItem>,
    private val listener: Adapter.ItemClickInterface,
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    val REPORT_WITH_IMAGE = 0
    val REPORT_WITH_NO_IMAGE = 1

    inner class ReportListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        private var dateText: TextView
        private var titleText: TextView
        private var stateText: TextView
        private var placeText: TextView

        init {
            itemView.setOnClickListener(this)
            this.dateText = itemView.recycle_item_no_image_date
            this.titleText = itemView.recycle_item_no_image_txt
            this.stateText = itemView.recycle_item_no_image_state_change
            this.placeText = itemView.recycle_item_no_image_place
        }

        fun bind(report: ReportListItem) {
            dateText.text = report.retriveDate(report.data)
            titleText.text = translate(report.anType)
            stateText.text = translate(report.state)
            placeText.text = report.place

        }

        override fun onClick(v: View?) {
            if (adapterPosition != RecyclerView.NO_POSITION)
                listener.onItemClick(adapterPosition)
        }


    }

    inner class ReportImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private var imgView: ImageView
        private var dateText: TextView
        private var titleText: TextView
        private var stateText: TextView
        private var placeText: TextView

        init {
            itemView.setOnClickListener(this)
            this.imgView = itemView.recycle_item_img
            this.dateText = itemView.recycle_item_date
            this.titleText = itemView.recycle_item_txt
            this.stateText = itemView.recycle_item_state_change
            this.placeText = itemView.recycle_item_place
        }

        fun bind(report: ReportListItem) {

            imgView.setImageBitmap(scaleToFitHeight(report.img!!))
            dateText.text = report.retriveDate(report.data)
            titleText.text = translate(report.anType)
            stateText.text = translate(report.state)
            placeText.text = report.place

        }

        override fun onClick(v: View?) {
            if (adapterPosition != RecyclerView.NO_POSITION)
                listener.onItemClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == REPORT_WITH_IMAGE) {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_view_report_item_with_image, parent, false)
            ReportImageViewHolder(itemView)
        } else {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_view_report_item_no_image, parent, false)
            ReportListViewHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currItem = reportList[position]
        if (getItemViewType(position) == REPORT_WITH_IMAGE)
            (holder as ReportImageViewHolder).bind(currItem)
        else {
            (holder as ReportListViewHolder).bind(currItem)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (reportList[position].img == null)
            REPORT_WITH_NO_IMAGE
        else
            REPORT_WITH_IMAGE
    }

    override fun getItemCount() = reportList.size

    private fun scaleToFitHeight(b: Bitmap): Bitmap {

        val factor = 120 / b.height.toFloat()
        return Bitmap.createScaledBitmap(b, (b.width * factor).toInt(), 80, true)
    }

    private fun translate(any: String): String {
        var str: String = ""
        when (any) {
            "CANE" -> str = context.resources.getString(R.string.cane)
            "GATTO" -> str = context.resources.getString(R.string.gatto)
            "TARTARUGA" -> str = context.resources.getString(R.string.tarta)
            "CONIGLIO" -> str = context.resources.getString(R.string.coni)
            "PAPPAGALLO" -> str = context.resources.getString(R.string.papp)
            "CRICETO" -> str = context.resources.getString(R.string.crice)
            "CANARINO" -> str = context.resources.getString(R.string.canar)
            "SERPENTE" -> str = context.resources.getString(R.string.serpe)
            "RAGNO" -> str = context.resources.getString(R.string.ragno)
            "GALLINA" -> str = context.resources.getString(R.string.galli)
            "APERTA" -> str = context.resources.getString(R.string.stato_aperta)
            "PRESA_IN_CARICO" -> str = context.resources.getString(R.string.presa_carico)
            "CHIUSA" -> str = context.resources.getString(R.string.chiusa)
        }
        return str
    }
}