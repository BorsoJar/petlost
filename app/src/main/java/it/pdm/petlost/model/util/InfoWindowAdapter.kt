package it.pdm.petlost.model.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.Adapter

class InfoWindowAdapter(context : Context, adapter : Adapter.InfoWindowInterface) : GoogleMap.InfoWindowAdapter {

    var adapter : Adapter.InfoWindowInterface
    lateinit var marker: Marker
    var ctx : View

    init{
        this.adapter = adapter
        ctx = (context as Activity).layoutInflater.inflate(R.layout.info_window_item,null)
    }



    override fun getInfoWindow(p0: Marker?): View {
        marker = p0!!
        connect(ctx)
        return ctx
    }

    override fun getInfoContents(p0: Marker?): View {
        marker = p0!!
        connect(ctx)
        return ctx
    }

    private fun connect(v : View){
        val title = v.findViewById<TextView>(R.id.win_title)
        title.text = marker.title
        }

}