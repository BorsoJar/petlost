package it.pdm.petlost.model.util

import com.google.android.gms.maps.model.LatLng
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.Report
import it.pdm.petlost.model.data.Stato

class FilterOrder(val map : MutableMap<String, Report>) {

    private var filtered : Boolean = false
    private var tmpmap : Map<String,Report> = mapOf()

    fun hasAlreadyFilter() : Boolean {
        return filtered
    }

    fun filterMapBy(filterPar: Any) : Map<String,Report>{
        filtered = true
        when (filterPar) {
            is Animale -> {
                tmpmap = map.filterValues {
                    it.animaleType == filterPar

                }
            }

            is Long -> {
                tmpmap = map.filterValues{
                    it.date>=filterPar
                }
            }

            is LatLng -> {
                tmpmap = map.filterValues {
                    val lat = it.site.lat
                    val lon = it.site.lng
                    lat>filterPar.latitude && lon> filterPar.longitude
                }
            }

            is Stato -> {
                tmpmap = map.filterValues {
                    it.reportState == filterPar
                }
            }
        }
        return tmpmap
    }
}