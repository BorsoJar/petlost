package it.pdm.petlost.model.data

import android.view.View
import android.widget.AdapterView
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.view.DefaultClusterRenderer

class ReportClusterItem(lat : Double, lon : Double, title: String, snippet : String?, repId : String) : ClusterItem {

    private val lat : Double
    private val lon : Double
    private val title : String
    private val snippet : String?
    private val repId : String
    init {
        this.lat = lat
        this.lon = lon
        this.title = title
        this.snippet = snippet
        this.repId = repId
    }

    override fun getPosition(): LatLng {
        return LatLng(lat, lon)
    }

    override fun getTitle(): String {
       return title
    }

    override fun getSnippet(): String? {
        return snippet
    }

    fun getReportId() : String {
        return repId
    }


}