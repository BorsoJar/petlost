package it.pdm.petlost.model.data


open class User() {
    var uid: String = ""
    var name: String = ""
    var surname: String = ""
    var email: String = ""
    var phone: String = ""
    var isAdmin: Boolean = false
    private var nick = ""

    constructor(email: String) : this() {
        this.email = email
    }

    constructor(
        uid: String,
        name: String,
        surname: String,
        email: String,
        phone: String,
        isAdmin: Boolean
    ) : this(email) {
        this.uid = uid
        this.name = surname
        this.phone = phone
    }

    override fun toString(): String {
        return "$uid,$name, $surname, $email, $phone, ${if (isAdmin) "admin" else "noAdmin"}"
    }

    fun getNick(): String {
        if (nick == "") {
            var tmps = ""
            for (c in email.trim()) {
                if (c != '@')
                    tmps += c
                else
                    break
            }
            return tmps

        }
        else
            return nick
    }

}