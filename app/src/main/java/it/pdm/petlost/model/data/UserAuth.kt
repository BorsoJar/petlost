package it.pdm.petlost.model.data

class UserAuth : User {
    private var psw: String

    constructor(email: String, psw: String) : super(email)
    {
        this.psw = psw
    }

    constructor(uid:String, nome: String, cognome: String, email: String, psw: String, cellulare: String) : super(uid,nome, cognome, email, cellulare, false)
    {
        this.psw = psw
    }

    fun setPsw(value : String)
    {
        psw = value
    }

    fun getPsw() : String
    {
        return psw
    }
}

