package it.pdm.petlost.model.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class Stato : Parcelable {
    APERTA,
    PRESA_IN_CARICO,
    CHIUSA
}

