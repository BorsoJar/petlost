package it.pdm.petlost.model

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.NotificationsInterface
import it.pdm.petlost.model.data.Report
import it.pdm.petlost.view.LogInActivity
import it.pdm.petlost.view.MainActivity
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

class NotificationsService : Service(), NotificationsInterface.Service {


    private lateinit var notificationManager: NotificationManager
    val CHANNEL_ID = "my_channel_id"
    val channel_name = "channel_name"
    val channel_description = "channel_description"
    private var SERVICE_ALREADY_ENABLED : Boolean = false

    var reportImage: Bitmap? = null
    lateinit var reportData : Pair<String, Report>

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate() {
        super.onCreate()
        PetLostManager.setNotificationsService(this)
        initializeNotificationManager()


    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if(SERVICE_ALREADY_ENABLED) {
            val prefs =
                getSharedPreferences(getString(R.string.sharedPrefences), Context.MODE_PRIVATE);
            val devoAttivareLeNotifiche =
                prefs.getBoolean(getString(R.string.notification_flag_userPreferences), false)

            if (!devoAttivareLeNotifiche ||  !checkisAdminFromSP()){
                PetLostManager.disableListenerForNewReportNotificationsRequest()
                stopSelf()
            }
        }
        else
        {
            SERVICE_ALREADY_ENABLED = true
            PetLostManager.getLastAddedReportRequest()
        }
        return super.onStartCommand(intent, flags, startId)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun initializeNotificationManager() {
        notificationManager = getSystemService(NotificationManager::class.java)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel: NotificationChannel = NotificationChannel(CHANNEL_ID, channel_name, NotificationManager.IMPORTANCE_DEFAULT)
            channel.setDescription(channel_description)
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun showNotification(
        error: NotificationsInterface.Error,
        newReport: Pair<String, Report>?,
        stateUpgrade: NotificationsInterface.Action?
    ) {
        lateinit var intent: Intent
        lateinit var pendingIntent: PendingIntent
        lateinit var  message : String
        when(error){
            NotificationsInterface.Error.NONE->
                {
                    val (key, report) = reportData

                    val bytearros = ByteArrayOutputStream()
                    reportImage?.compress(Bitmap.CompressFormat.JPEG, 10, bytearros)



                    val pendingIntent = createPendingintent(createBundle(newReport!!))

                    val builder: NotificationCompat.Builder =
                        NotificationCompat.Builder(this, CHANNEL_ID)
                            .setSmallIcon(android.R.drawable.star_on)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent)

                    if(stateUpgrade != null)
                        when(stateUpgrade)
                        {
                            NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_CLOSED -> message = getString(R.string.notification_stateUpgradeToClosed_text)
                            NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_PROGRESS -> message = getString(R.string.notification_stateUpgradeToProgress_text)
                        }
                    else
                        message = getString(R.string.notification_new_report_text)

                    builder.setContentTitle(getString(R.string.app_name))
                        .setContentText(message)
//                            .setContentTitle(getString(R.string.notification_new_report))
//                            .setContentText("${report.animaleType} - ${report.site.convertLatLngToAddress(report.site, this)}")


//                    if(reportImage != null)
//                    {
//                        builder.setLargeIcon(reportImage)
//                            .setStyle(NotificationCompat.BigPictureStyle()
//                                .bigPicture(reportImage)
//                                .bigLargeIcon(null))
//                    }

                    notificationManager.notify(0, builder.build())

                }
            NotificationsInterface.Error.USER_DISCONNECTED ->
                {
                    intent = Intent(this, LogInActivity::class.java)
                    pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

                    val  builder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(android.R.drawable.star_on)
                        .setContentTitle(getString(R.string.notification_user_disconnected_title))
                        .setContentText(getString(R.string.notification_user_disconnected_text))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)

                    notificationManager.notify(0, builder.build())
                }
        }



    }

    override fun onSuccess(
        action: NotificationsInterface.Action,
        obj: Any?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when(action){
            NotificationsInterface.Action.ON_NEW_REPORT ->
            {
                this.reportData = obj as Pair<String, Report>
                PetLostManager.downloadReportImageForNotificationRequest(reportData.first, NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_SERVICE)
            }
            NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_SERVICE ->
            {
                this.reportImage = obj as Bitmap
                showNotification(NotificationsInterface.Error.NONE, reportData, null)
            }
            NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_PROGRESS, NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_CLOSED ->
            {
                this.reportData = obj as Pair<String, Report>
                this.reportImage = null
                showNotification(NotificationsInterface.Error.NONE, reportData, action)
            }

        }
    }

    override fun onFail(
        error: NotificationsInterface.Error,
        extraInfo: NotificationsInterface.Action?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when(error){
            NotificationsInterface.Error.USER_DISCONNECTED ->showNotification(error, null, null)
            NotificationsInterface.Error.ERROR_DATABASE_READING ->Log.e("NotificationsService", "Errore lettura report dal database.")
            NotificationsInterface.Error.ERROR_INIT_NOTIFICATIONS_SERVICE-> Log.e("NotificationsService", "Errore inizializzazione servizio")
            NotificationsInterface.Error.FAILED_DOWNLOAD_REPORT_IMAGE, NotificationsInterface.Error.REPORT_IMAGE_DEOSNT_EXIST ->
            {
                Log.e("NotificationsService", "Non è stato possibile scaricare la report image")
                this.reportImage = null
                showNotification(NotificationsInterface.Error.NONE, reportData, null)
            }
            NotificationsInterface.Error.STATE_UPGRADE -> Log.e("NotificationsService", "Errore durante elaborazione informazioni per notifica di state upgrade oppure transizione irrilevante")
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        SERVICE_ALREADY_ENABLED = false

    }

    private fun createBundle(newReport: Pair<String, Report>): Bundle
    {
        val (key, report) = reportData
        val bundle = Bundle()
        bundle.putString("reportId", key)
        bundle.putString("desc", report.desc)
        bundle.putString("date", SimpleDateFormat("dd.MM.yyyy HH:mm").format(Date(report.date)))
        bundle.putBoolean("user", false) //non serve perchè chi riceve le notifiche non è mail l'autore
        bundle.putString("authorID", report.authorID)
        bundle.putString("place", report.site.convertLatLngToAddress(report.site, this))
        bundle.putString("animal", report.animaleType.toString())
        bundle.putString("state", report.reportState.toString())
        bundle.putParcelable("img", null)
        return bundle
    }

    private fun createPendingintent(bundle: Bundle) : PendingIntent
    {
        val pendingIntent = NavDeepLinkBuilder(this)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.nav_graph)
            .setDestination(R.id.reportItemFragment)
            .setArguments(bundle)
            .createPendingIntent()
        return pendingIntent
    }

    private fun checkisAdminFromSP(): Boolean
    {
        val prefs = getSharedPreferences(getString(R.string.sharedPrefences), Context.MODE_PRIVATE);
        return prefs.getBoolean(getString(R.string.notification_flag_isAdmin), false)
    }
}