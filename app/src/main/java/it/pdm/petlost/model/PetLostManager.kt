package it.pdm.petlost.model

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.startActivity
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseException
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.*
import it.pdm.petlost.model.data.*
import it.pdm.petlost.model.db.FirebaseController
import it.pdm.petlost.model.interfacesModel.PetLostManagerInterface
import it.pdm.petlost.model.util.FilterOrder
import it.pdm.petlost.model.util.CommentSort
import it.pdm.petlost.model.util.ReportSort
import it.pdm.petlost.presenter.CommentsPresenter
import java.util.*
import kotlin.collections.ArrayList


object PetLostManager : PetLostManagerInterface,
                        MapsReportInterface.ModelReport,
                        ProfileInterfaces.Model,
                        NewReportInterface.ModelNewReport,
                        NewReportInterface.OnNewReportCompleteListener,
                        ReportListInterface.ModelReport,
                        PetLostManagerInterface.Listener,
                        MyReportInterface.MyModelReport,
                        MainActivityInterface.MainActivityModel,
                        CommentsInterface.ModelInterface,
                        SettingsInterface.Model,
                        NotificationsInterface.Model{


    private val firebaseCtrl = FirebaseController
    private var reportHashMap: MutableMap<String, Report>
    private var imgHashMap: MutableMap<String, Bitmap>
    private var commentHashMap: MutableMap<String, MutableMap<String, Comment>>
    lateinit var user: User
    private var userProfilePhoto: Bitmap? = null

    private lateinit var profilePresenter: ProfileInterfaces.Presenter
    private lateinit var settingsPresenter: SettingsInterface.Presenter
    private var mapsPresenter: MapsReportInterface.PresenterReport? = null
    private lateinit var newReportPresenter: NewReportInterface.PresenterNewReport
    private var mainActivityPresenter: MainActivityInterface.MainActivityPresenter? = null
    private var reportListPresenter: ReportListInterface.PresenterReportList? = null
    private var reportItemPresenter: ReportListInterface.PresenterReportItem? = null
    private var commentPresenter: CommentsInterface.PresenterInterface? = null
    private var myRepListPresenter: MyReportInterface.MyRepPresenter? = null
    private lateinit var notificationsService: NotificationsInterface.Service

    private var commentSort : CommentSort
    private var reportSort : ReportSort

    init {
        reportHashMap = mutableMapOf()
        imgHashMap = mutableMapOf()
        commentHashMap = mutableMapOf()
        commentSort = CommentSort()
        reportSort = ReportSort()
    }

    /*------------------------------- SETTING ALL PRESENTERS -------------------------------*/

    override fun setMapsPresenter(mapsPresenter: MapsReportInterface.PresenterReport): MapsReportInterface.ModelReport {
        this.mapsPresenter = mapsPresenter
        FirebaseController.updateMapsData(this)
        return this
    }

    override fun setNewReportPresenter(newReportPresenter: NewReportInterface.PresenterNewReport): NewReportInterface.ModelNewReport {
        this.newReportPresenter = newReportPresenter
        return this
    }

    override fun setProfilePresenter(profilePresenter: ProfileInterfaces.Presenter): ProfileInterfaces.Model {
        this.profilePresenter = profilePresenter
        return this
    }

    override fun setNotificationsService(notificationsService: NotificationsInterface.Service) {
        this.notificationsService = notificationsService
    }

    override fun setMainActivityPresenter(mainActivityPresenter: MainActivityInterface.MainActivityPresenter): MainActivityInterface.MainActivityModel {
        this.mainActivityPresenter = mainActivityPresenter
        firebaseCtrl.openConnection()
        return this
    }

    override fun setCommentsPresenter(commentsPresenter: CommentsPresenter): CommentsInterface.ModelInterface {
        this.commentPresenter = commentsPresenter
        return this
    }

    override fun setMyRepListPresenter(myRepListPresenter: MyReportInterface.MyRepPresenter?): MyReportInterface.MyModelReport {
        this.myRepListPresenter = myRepListPresenter
        return this
    }

    override fun setReportListPresenter(reportListPresenter: ReportListInterface.PresenterReportList?): ReportListInterface.ModelReport {
        this.reportListPresenter = reportListPresenter
        return this
    }

    override fun setReportItemPresenter(reportItemPresenter: ReportListInterface.PresenterReportItem): ReportListInterface.ModelReport {
        this.reportItemPresenter = reportItemPresenter
        return this
    }

    override fun setSettingsPresenter(settingsPresenter: SettingsInterface.Presenter): SettingsInterface.Model {
        this.settingsPresenter = settingsPresenter
        return this
    }

    override fun deleteMainActivityPresenter() {
        this.mainActivityPresenter = null
    }

    override fun deleteReportItemPresenter(){
        reportItemPresenter = null
    }

    override fun deleteMapsPresenter() {
        mapsPresenter = null
    }

    override fun deleteCommentPresenter() {
        commentPresenter = null
    }

    override fun deleteListReportPresenter() {
        reportListPresenter = null
    }

    override fun deleteMyRepListPresenter() {
        myRepListPresenter = null
    }


    override fun removeListener(reportId: String) {
        firebaseCtrl.deleteListener(reportId)
    }


    /*-------------------------------   LIST METHOD     -------------------------------*/

    @RequiresApi(Build.VERSION_CODES.N)
    override fun filterReportBy(address: LatLng?, animale: Animale?, data: Long?, dataTo: Long?, range: Double, reportState: Stato?, context: Context): List<ReportListItem> {
        var order = FilterOrder(reportHashMap)
        var tmpmap : Map<String,Report> = mapOf()
        var mutablemap : MutableMap<String,Report> = mutableMapOf()
        if(animale != null) {
            tmpmap = order.filterMapBy(animale)
            mutablemap = tmpmap.toMutableMap()
        }
        if(data != null) {
            if (order.hasAlreadyFilter()) {
                order = FilterOrder(tmpmap.toMutableMap())
                tmpmap = order.filterMapBy(data)
                if (dataTo != null) {
                    mutablemap = tmpmap.filterValues {
                        it.date<dataTo
                    } as MutableMap<String, Report>
                }
            } else {
                tmpmap = order.filterMapBy(data)
                if (dataTo != null && tmpmap.isNotEmpty()) {
                    mutablemap = tmpmap.filterValues {
                        it.date<dataTo
                    } as MutableMap<String, Report>
                }
            }
        }
        if(address != null){
            if (order.hasAlreadyFilter()) {
                order = FilterOrder(tmpmap.toMutableMap())
                tmpmap =
                    order.filterMapBy(LatLng(address.latitude - range, address.longitude - range))
                if (!tmpmap.isNullOrEmpty()) {
                    mutablemap = tmpmap.filterValues {
                        val lat = it.site.lat
                        val lon = it.site.lng
                        lat < address.latitude+range && lon < address.longitude+range
                    } as MutableMap<String, Report>
                }
            }
            else{
               tmpmap=  order.filterMapBy(LatLng(address.latitude - range, address.longitude - range))
                if (!tmpmap.isNullOrEmpty()) {
                    mutablemap = tmpmap.filterValues {
                        val lat = it.site.lat
                        val lon = it.site.lng
                        lat < address.latitude+range && lon < address.longitude+range
                    } as MutableMap<String, Report>
                }

            }

        }
        if(reportState != null){
            if(order.hasAlreadyFilter()){
                order = FilterOrder(tmpmap.toMutableMap())
                tmpmap = order.filterMapBy(reportState)
            }
            else
                tmpmap = order.filterMapBy(reportState)
            mutablemap = tmpmap.toMutableMap()
        }

        val tmparr = arrayListOf<ReportListItem>()
        if(!mutablemap.isNullOrEmpty()) {
            for (item in mutablemap) {
                    val tmprep = ReportListItem(
                        item.key,
                        item.value.authorID,
                        item.value.site.toString(),
                        item.value.date,
                        item.value.desc,
                        item.value.animaleType.toString(),
                        item.value.reportState.toString(),
                        null
                    )

                if(item.value.photoID!= "")
                    tmprep.img = imgHashMap[item.value.photoID]
                tmparr.add(tmprep)
            }
        }
        return tmparr
    }


    /*-------------------------------   REPORT METHOD     -------------------------------*/

    override fun addNewReportRequest(report: Report, uuid: UUID) {
        report.authorID = user.uid
        firebaseCtrl.createReport(report, this,uuid)
    }

    override fun onCreationReportSuccess() {
        newReportPresenter.onCreationReportSuccess()
    }

    override fun onCreationReportFailure() {
        newReportPresenter.onCreationReportFailure()
    }

    override fun getReportsDataForItem(id: String, context: Context): ReportListItem? {
        var tmplistrep: ReportListItem? = null
        if (reportHashMap.containsKey(id)) {
            val tmprep = reportHashMap[id]
            if (!id.equals("") && reportHashMap.isNotEmpty() && tmprep != null) {
                tmplistrep = ReportListItem(
                    id,
                    tmprep.authorID,
                    tmprep.site.convertLatLngToAddress(tmprep.site, context),
                    tmprep.date,
                    tmprep.desc,
                    tmprep.animaleType.toString(),
                    tmprep.reportState.toString(),
                    imgHashMap[tmprep.photoID]
                )
            }
        }
        return tmplistrep
    }

    override fun getReportsDataForList(id: Boolean): List<ReportListItem> {
        var mtbList = mutableListOf<ReportListItem>()
        var imgtmp : Bitmap?
        if (id) {
            for (item in reportHashMap) {
                imgtmp = null
                val photoId = item.value.photoID
                if(photoId != ""){
                    if(imgHashMap.containsKey(photoId))
                        imgtmp = imgHashMap[photoId]
                    else
                        firebaseCtrl.downloadImage(photoId,this)
                }
                val userId = firebaseCtrl.getUserId()
                if (item.value.authorID.equals(userId)) {
                    mtbList.add(
                        ReportListItem(
                            item.key,
                            userId,
                            item.value.site.toString(),
                            item.value.date,
                            item.value.desc,
                            item.value.animaleType.toString(),
                            item.value.reportState.toString(),
                            imgtmp
                        )
                    )

                }
            }
        } else {
            for (item in reportHashMap) {
                imgtmp = null
                val photoId = item.value.photoID
                if (photoId != "") {
                    if (imgHashMap.containsKey(photoId))
                        imgtmp = imgHashMap[photoId]
                    else
                        firebaseCtrl.downloadImage(photoId, this)
                }
                mtbList.add(
                    ReportListItem(
                        item.key,
                        item.value.authorID,
                        item.value.site.toString(),
                        item.value.date,
                        item.value.desc,
                        item.value.animaleType.toString(),
                        item.value.reportState.toString(),
                        imgtmp
                    )
                )

            }
        }
        mtbList = reportSort.quickSort(mtbList as ArrayList<ReportListItem>,0,mtbList.size-1)
        return mtbList
    }

    override fun onReportSuccess(data: DataSnapshot) {
        for (item: DataSnapshot in data.children) {
            val tmp = item.getValue(Report::class.java)
            Log.i("Firebasectrl", tmp.toString())
            if (tmp?.photoID != null && tmp.photoID != "") {
                firebaseCtrl.downloadImage(tmp.photoID, this)
            }
            reportHashMap.put(item.key.toString(), tmp!!)
        }
        onDataChange(null)
    }

    override fun deleteReport(reportId: String) {
        val idList = arrayListOf<String>()
        if (commentHashMap.isNotEmpty() && commentHashMap.contains(reportId)) {
            commentHashMap.remove(reportId)
        }
        firebaseCtrl.deleteComment(reportId, this)
        firebaseCtrl.deleteReport(reportId, idList, this)
        reportHashMap.remove(reportId)
        onDataChange(null)
    }

    override fun onDeleteReport(codeR: Int, codeC: Int) {
        if (codeC >= 0 && codeR >= 0 && reportItemPresenter != null)
            reportItemPresenter!!.onDeleteSuccess()
        else
            reportItemPresenter!!.onDeleteFailed()
    }


    override fun updateReportState(reportId: String, state: Stato) {
        if(reportHashMap.containsKey(reportId)) {
            reportHashMap[reportId]!!.reportState = state
            firebaseCtrl.updateReportState(reportId,state)
        }
    }

    /*-------------------------------   USERS METHOD     -------------------------------*/

    override fun requestUserInfo() {
        firebaseCtrl.getUserInfo(this)
    }

    override fun requestLogout(context: Context) {
        firebaseCtrl.logOut(this)
        val editor: SharedPreferences.Editor = context.getSharedPreferences(context.getString(R.string.sharedPrefences), MODE_PRIVATE).edit()
        editor.remove(context.getString(R.string.login_sharedPreferencesCredentials_email))
        editor.remove(context.getString(R.string.login_sharedPreferencesCredentials_password))
        editor.apply()
        context.cacheDir.deleteRecursively()
    }



    override fun onSuccess(requestCode: MainActivityInterface.ActionCode, value: Any?) {
        when (requestCode) {
            MainActivityInterface.ActionCode.USER_INFO -> {
                this.user = value!! as User
                firebaseCtrl.downloadUserProfilePhoto(user.uid, this)
                mainActivityPresenter!!.onSuccess(MainActivityInterface.ActionCode.USER_INFO, null)
            }
            MainActivityInterface.ActionCode.LOGOUT -> mainActivityPresenter!!.onSuccess(
                MainActivityInterface.ActionCode.LOGOUT,
                null
            )
            MainActivityInterface.ActionCode.USER_PHOTO -> {
                val byte: ByteArray = value!! as ByteArray
                val bitmap = BitmapFactory.decodeByteArray(byte, 0, byte.size)
                userProfilePhoto = bitmap
                mainActivityPresenter!!.onSuccess(MainActivityInterface.ActionCode.USER_PHOTO, null)
            }
        }

    }

    override fun onFailure(requestCode: MainActivityInterface.ActionCode) {
        mainActivityPresenter?.onFailure(requestCode)
    }

    override fun getCurrentUser(): String {
        return user.uid
    }

    override fun getUserReference(): User {
        return user
    }

    override fun getUserPhoto(): Bitmap? {
        return userProfilePhoto
    }

    override fun checkAdmin(): Boolean {
        return user.isAdmin
    }

    /*-------------------------------   IMAGE METHOD     -------------------------------*/

    override fun addNewImage(image: Bitmap, uuid: UUID) {
        firebaseCtrl.uploadImage(uuid, image, this)
    }

    override fun uploadImageSuccess() {
        Log.i("PetLostManager", "UploadImageSuccess")
    }

    override fun uploadImageFailed() {
        Log.e("PetLostManager", "UploadImageFailed")
    }

    override fun onImageSuccess(id: String, byte: ByteArray) {
        val bitmap = BitmapFactory.decodeByteArray(byte, 0, byte.size)
        imgHashMap[id] = bitmap

    }


    /*-------------------------------   COMMENT METHOD     -------------------------------*/

    override fun getCommentsReport(repId: String): List<CommentListItem>? {
        var tmpcommlist: List<CommentListItem>? = null
        var tmpcommimg: Bitmap?
        var tmpcom: CommentListItem
        if (commentHashMap.containsKey(repId)) {
            tmpcommlist = arrayListOf()
            val commentmap = commentHashMap[repId]
            if (commentmap != null) {
                for (item in commentmap) {
                    if (item.value.photoID != null && !item.value.photoID.equals("")) {
                        if (!imgHashMap.contains(item.value.photoID))
                            firebaseCtrl.downloadCommentImage(item.value.photoID!!, this, repId)
                        tmpcommimg = imgHashMap[item.value.photoID]

                    }
                    else
                        tmpcommimg = null

                    tmpcom = CommentListItem(
                        item.value.commId,
                        item.value.authorID,
                        item.value.authornick,
                        item.value.date,
                        item.value.description,
                        tmpcommimg
                    )

                    tmpcommlist.add(tmpcom)
                }
            }
        }

        return tmpcommlist
    }

    override fun onCommentDeletedSuccess() {
        Log.i("FirebaseController", "Comment deleated")
    }

    override fun onCommentDeletedFailed(it: java.lang.Exception) {
        Log.e("FirebaseController", it.toString())
    }

    override fun assignListener(reportId: String) {
        firebaseCtrl.updateCommentsData(this, reportId)
    }

    override fun writeComment(text: String, reportId: String) {
        val currentDate = Calendar.getInstance().timeInMillis
        val uuid: UUID = UUID.randomUUID()
        val tmpcomm = Comment(user.uid, currentDate, text, user.getNick(), reportId, null, uuid.toString())

        if (!commentHashMap.containsKey(reportId)) {
            val tmpmap = mutableMapOf<String, Comment>()
            tmpmap.put(uuid.toString(), tmpcomm)
            commentHashMap.put(reportId, tmpmap)
        } else
            commentHashMap[reportId]!!.put(uuid.toString(), tmpcomm)
        firebaseCtrl.writeComment(commentHashMap, reportId, this)
    }

    override fun writeCommentWithImage(reportId: String, text: String, img: Bitmap) {
        val currentDate = Calendar.getInstance().timeInMillis
        val commuuid: UUID = UUID.randomUUID()
        val tmpcomm =
            Comment(user.uid, currentDate, text, user.getNick(), reportId, commuuid.toString(), commuuid.toString())
        if (!commentHashMap.containsKey(reportId)) {
            val tmpmap = mutableMapOf<String, Comment>()
            tmpmap.put(commuuid.toString(), tmpcomm)
            commentHashMap.put(reportId, tmpmap)
        } else
            commentHashMap[reportId]!!.put(commuuid.toString(), tmpcomm)
        imgHashMap.put(commuuid.toString(), img)
        firebaseCtrl.writeCommentWithImage(commentHashMap, reportId, commuuid.toString(), img, this)
    }

    override fun onWriteFailed(err: String) {
        Log.e("OnWriteComment", err)
    }

    override fun uploadCommentWithImageSuccess() {
        Log.i("CommentWIthImage", "Image uploaded success")
    }

    override fun uploadCommentWithImageFailed(exception: java.lang.Exception?) {
        Log.e("OnWriteComment", exception?.message.toString())
    }

    override fun onCommentChangeFailed(toException: DatabaseException) {
        Log.e("onCommentChange", toException.toString())
    }

    override fun onCommentChange(dataSnapshot: DataSnapshot) {
        var repid = ""
        for (item: DataSnapshot in dataSnapshot.children) {
            val tmp = item.getValue(Comment::class.java)
            repid = tmp!!.reportId
            if (tmp.photoID != null && tmp.photoID != "") {
                if (!imgHashMap.contains(tmp.commId)) {
                    firebaseCtrl.downloadCommentImage(tmp.photoID.toString(), this, repid)
                }
                repid = tmp.reportId
            }
            if (commentHashMap.containsKey(repid)) {
                if (!commentHashMap.containsValue(tmp.commId))
                    commentHashMap[repid]!!.put(tmp.commId, tmp)

            } else {
                val tmpmap = mutableMapOf<String, Comment>()
                tmpmap.put(tmp.commId, tmp)
                commentHashMap.put(tmp.reportId, tmpmap)
            }
            //commentPresenter!!.commentError(COMMENT_CHANGE_ERR)
        }
        onDataChange(repid)
    }

    override fun onWriteComplete(reportId: String) {
        commentPresenter!!.reloadInfo(getCommentsReport(reportId))
    }

    override fun onFailed(e: Exception) {
        Log.e("PetLostManager", "Impossible to update data, try later... ${e.message.toString()}")
    }

    override fun onCommentImageDownloaded(image: Bitmap?, imgId: String, repId: String) {
        if (image != null) {
            imgHashMap[imgId] = image
            onDataChange(repId)
        }
    }

    /*-------------------------------   PROFILE METHOD     -------------------------------*/

    override fun onSuccess(requestCode: ProfileInterfaces.ActionCode) {
        requestUserInfo()
        when (requestCode) {
            ProfileInterfaces.ActionCode.NAME_UPDATE -> profilePresenter.onSuccess(ProfileInterfaces.ActionCode.NAME_UPDATE)
            ProfileInterfaces.ActionCode.SURNAME_UPDATE -> profilePresenter.onSuccess(
                ProfileInterfaces.ActionCode.SURNAME_UPDATE
            )
            ProfileInterfaces.ActionCode.PHONE_UPDATE -> profilePresenter.onSuccess(
                ProfileInterfaces.ActionCode.PHONE_UPDATE
            )
            ProfileInterfaces.ActionCode.PHOTO_UPDATE -> profilePresenter.onSuccess(
                ProfileInterfaces.ActionCode.PHOTO_UPDATE
            )
            ProfileInterfaces.ActionCode.PASSWORD_UPDATE -> profilePresenter.onSuccess(
                ProfileInterfaces.ActionCode.PASSWORD_UPDATE
            )
        }
    }

    override fun onFailure(requestCode: ProfileInterfaces.ActionCode, errorMessage: ProfileInterfaces.ErrorCode) {
        when (requestCode) {
            ProfileInterfaces.ActionCode.NAME_UPDATE -> profilePresenter.onFailure(
                ProfileInterfaces.ActionCode.NAME_UPDATE,
                errorMessage
            )
            ProfileInterfaces.ActionCode.SURNAME_UPDATE -> profilePresenter.onFailure(
                ProfileInterfaces.ActionCode.SURNAME_UPDATE,
                errorMessage
            )
            ProfileInterfaces.ActionCode.PHONE_UPDATE -> profilePresenter.onFailure(
                ProfileInterfaces.ActionCode.PHONE_UPDATE,
                errorMessage
            )
            ProfileInterfaces.ActionCode.PHOTO_UPDATE -> profilePresenter.onFailure(
                ProfileInterfaces.ActionCode.PHOTO_UPDATE,
                errorMessage
            )
            ProfileInterfaces.ActionCode.PASSWORD_UPDATE -> profilePresenter.onFailure(
                ProfileInterfaces.ActionCode.PASSWORD_UPDATE,
                errorMessage
            )
        }
    }

    override fun sendAdminRequest(request: String, context: Context): SettingsInterface.ErrorCode {
        lateinit var result: SettingsInterface.ErrorCode
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.setData(Uri.parse("mailto:"))
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(context.getString(R.string.application_serviceMail)))
        intent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.adinRequest_emailObject) + user.uid)
        intent.putExtra(Intent.EXTRA_TEXT, request)

        try {
            startActivity(context, intent, null)
            result = SettingsInterface.ErrorCode.ADMIN_REQUEST_SUCCESS
        } catch (ex: java.lang.Exception) {
            result = SettingsInterface.ErrorCode.ADMIN_REQUEST_FAIL
        }
        return result
    }

    override fun nameChangeRequest(newName: String, resultHandler: ProfileInterfaces.onCompleteAction) {
        firebaseCtrl.updataUserName(newName, this)
    }

    override fun surnameChangeRequest(newSurname: String, resultHandler: ProfileInterfaces.onCompleteAction) {
        firebaseCtrl.updataUserSurname(newSurname, this)
    }

    override fun photoChangeRequest(newPhoto: Bitmap, resultHandler: ProfileInterfaces.onCompleteAction) {
        firebaseCtrl.uploadUserProfilePhoto(newPhoto, this)
    }

    override fun phoneChangeRequest(newPhone: String, resultHandler: ProfileInterfaces.onCompleteAction) {
        firebaseCtrl.updataUserPhone(newPhone, this)
    }

    override fun passwordChangeRequest(hashOldPsw: String, hashNewPsw: String, resultHandler: ProfileInterfaces.onCompleteAction) {
        firebaseCtrl.changePassword(user.email, hashOldPsw, hashNewPsw, this)
    }

    /*-------------------------------   DATA CHANGE METHOD     -------------------------------*/

    override fun onDataChange(reportId: String?) {
        if (commentPresenter != null && reportId != null) {
            val tmplist: ArrayList<CommentListItem> = arrayListOf()
            if (commentHashMap.containsKey(reportId) && commentHashMap[reportId]!!.isNotEmpty()) {
                for (item in commentHashMap[reportId]!!.values) {
                    if (item.photoID != null && item.photoID != "") {
                        val imageId = item.photoID!!
                        if (!imgHashMap.containsKey(imageId)) {
                            firebaseCtrl.downloadCommentImage(imageId, this, reportId)
                        }

                        tmplist.add(
                            CommentListItem(
                                item.commId,
                                item.authorID,
                                item.authornick,
                                item.date,
                                item.description,
                                imgHashMap[imageId]
                            )
                        )
                    } else
                        tmplist.add(
                            CommentListItem(
                                item.commId,
                                item.authorID,
                                item.authornick,
                                item.date,
                                item.description,
                                null
                            )
                        )
                }
            }
            commentPresenter!!.reloadInfo(commentSort.quickSort(tmplist,0, tmplist.size-1))
        }
        if (mapsPresenter != null) {
            val mtbList = mutableListOf<ReportsMapsItem>()
            for (item in reportHashMap) {
                mtbList.add(
                    ReportsMapsItem(
                        item.value.animaleType.toString(),
                        item.value.site,
                        item.key
                    )
                )
            }
            mapsPresenter!!.updateData(mtbList)
        }

        if(reportListPresenter != null){
            var tmplist = getReportsDataForList(false)
            tmplist = reportSort.quickSort(tmplist as ArrayList<ReportListItem>,0,tmplist.size-1)
            reportListPresenter!!.updateData(tmplist)
        }

        if(myRepListPresenter!= null)
            myRepListPresenter!!.updateData(getReportsDataForList(true))

    }

    override fun getLastAddedReportRequest() {
        firebaseCtrl.getLastAddedReport(this)
    }

    override fun downloadReportImageForNotificationRequest(
        reportID: String,
        action: NotificationsInterface.Action
    ) {
        firebaseCtrl.downloadReportImageForNotification(reportID, this, action)
    }

    override fun isAdminCheckRequestForNotification(broadcastReceiver: NotificationsInterface.NewReportListener) {
        firebaseCtrl.queryCurrentUserToCheckIfHeIsAdmin(this, broadcastReceiver)

    }


    override fun isNotificationActivated(context: Context): Boolean
    {
        val prefs = context.getSharedPreferences(context.getString(R.string.sharedPrefences), Context.MODE_PRIVATE);
        return prefs.getBoolean(context.getString(R.string.notification_flag_userPreferences), false)
    }

    override fun disableListenerForNewReportNotificationsRequest() {
        firebaseCtrl.disableListenerForNewReportNotifications()
    }

    override fun onSuccess(
        action: NotificationsInterface.Action,
        obj: Any?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when(action){
            NotificationsInterface.Action.ON_NEW_REPORT -> notificationsService.onSuccess(
                NotificationsInterface.Action.ON_NEW_REPORT,
                obj as Pair<String, Report>,
                null
            )
            NotificationsInterface.Action.GETTING_LAST_ADDED_REPORT ->{
                firebaseCtrl.onNewReport(this, obj as Long)
            }
            NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_SERVICE -> notificationsService.onSuccess(
                NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_SERVICE,
                obj as Bitmap,
                null
            )
            NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_PRESENTER -> reportItemPresenter!!.onSuccess(
                NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_PRESENTER,
                obj as Bitmap,
                null
            )
            NotificationsInterface.Action.SHOULD_NOTIFICATIONS_BE_ACTIVATED -> broadcastReceiver!!.onSuccess(action, obj as Boolean, null)
            NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_PROGRESS ->notificationsService.onSuccess(NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_PROGRESS, obj as Pair<String, Report>, null)
            NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_CLOSED ->notificationsService.onSuccess(NotificationsInterface.Action.REPORT_STATE_UPGRADE_TO_CLOSED, obj as Pair<String, Report>, null)

        }
    }

    override fun onFail(
        error: NotificationsInterface.Error,
        extraInfo: NotificationsInterface.Action?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when(extraInfo){
            NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_SERVICE ->notificationsService.onFail(
                error,
                null,
                null
            )
            NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_PRESENTER -> reportItemPresenter!!.onFail(
                error,
                null,
                null
            )
            NotificationsInterface.Action.SHOULD_NOTIFICATIONS_BE_ACTIVATED -> broadcastReceiver!!.onSuccess(extraInfo, false, null)

        }
        when(error)
        {
            NotificationsInterface.Error.STATE_UPGRADE -> notificationsService.onFail(NotificationsInterface.Error.STATE_UPGRADE, null, null)
        }

    }


    override fun notificationsActivation(
        context: Context,
        enabled: Boolean
    ) {
        val prefs = context.getSharedPreferences(context.getString(R.string.sharedPrefences), Context.MODE_PRIVATE)
        prefs.edit().putBoolean(context.getString(R.string.notification_flag_userPreferences), enabled).apply()
    }



}