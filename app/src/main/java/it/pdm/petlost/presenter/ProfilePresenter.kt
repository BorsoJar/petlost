package it.pdm.petlost.presenter

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.ProfileInterfaces
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.util.PasswordUtil
import it.pdm.petlost.model.util.RegexUtil

class ProfilePresenter : ProfileInterfaces.Presenter, ProfileInterfaces.onCompleteAction {
    private var view: ProfileInterfaces.View
    private var model: ProfileInterfaces.Model

    constructor(v: ProfileInterfaces.View)
    {
        view = v
        model = PetLostManager.setProfilePresenter(this)
    }

    override fun getName(): String {
        return model.getUserReference().name
    }

    override fun getSurname(): String {
        return model.getUserReference().surname
    }

    override fun getPhone(): String {
        return model.getUserReference().phone
    }

    override fun getPhoto(): Bitmap {
        return model.getUserPhoto()!!
    }

    override fun getEmail(): String {
        return model.getUserReference().email
    }

    override fun isAdmin(): Boolean {
        TODO("Not yet implemented")
    }

    override fun hasPhoto(): Boolean {
        return model.getUserPhoto() != null
    }

    override fun whichDataHasChanged(newName: String, newSurname: String, newPhone: String, newPhoto: Bitmap): BooleanArray {
        var ris = BooleanArray(4) //inizializzati tutti a false
        if(isNameChanged(newName))
            ris[0] = true
        if(isSurnameChanged(newSurname))
            ris[1] = true
        if(isPhoneChanged(newPhone))
            ris[2] = true
        if(isPhotoChanged(newPhoto))
            ris[3] = true
        return ris
    }

    override fun tryToChangeName(
            newName: String
    ) {
        model.nameChangeRequest(newName, this)
    }

    override fun tryToChangeSurname(
            newSurname: String
    ) {
        model.surnameChangeRequest(newSurname, this)
    }

    override fun tryToChangePhone(
            newPhone: String
    ) {
        model.phoneChangeRequest(newPhone, this)
    }

    override fun tryToChangePhoto(
            newPhoto: Bitmap
    ) {
        model.photoChangeRequest(newPhoto, this)
    }

    override fun tryToChangePassword(
            oldPsw: String,
            newPsw: String
    ) {
        model.passwordChangeRequest(PasswordUtil.encrypt(oldPsw), PasswordUtil.encrypt(newPsw), this)
    }

    override fun checkPassword(oldPsw: String, newPsw: String, newPswConfirmation: String): Int {
        return when{
            oldPsw.isNullOrEmpty() -> 1
            newPsw.isNullOrEmpty() -> 2
            newPswConfirmation.isNullOrEmpty() -> 3
            !newPsw.equals(newPswConfirmation) -> 4
            !RegexUtil.verifyPwPattern(newPsw) -> 5
            else -> 0
        }

    }

    private fun isNameChanged(newName: String) : Boolean
    {
        return (newName != getName())
    }

    private fun isSurnameChanged(newSurname: String) : Boolean
    {
        return (newSurname != getSurname())
    }

    private fun isPhoneChanged(newPhone: String) : Boolean
    {
        return (newPhone != getPhone())
    }

    private fun isPhotoChanged(newPhoto: Bitmap) : Boolean
    {
        return if(hasPhoto())
            !(newPhoto.sameAs(model.getUserPhoto()))
        else {
            !(newPhoto.sameAs(BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.ic_baseline_camera_alt_24)))
        }
    }

    override fun onSuccess(requestCode: ProfileInterfaces.ActionCode) {
        view.onSuccess(requestCode)
    }

    override fun onFailure(requestCode: ProfileInterfaces.ActionCode, errorMessage: ProfileInterfaces.ErrorCode) {
        view.onFailure(requestCode, errorMessage)
    }

}