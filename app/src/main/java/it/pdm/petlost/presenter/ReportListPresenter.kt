package it.pdm.petlost.presenter

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import it.pdm.petlost.interfaces.ReportListInterface
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.ReportListItem
import it.pdm.petlost.model.data.Stato

class ReportListPresenter(val view : ReportListInterface.ViewReportList) : ReportListInterface.PresenterReportList {
    private val model : ReportListInterface.ModelReport


    init {
        model = PetLostManager.setReportListPresenter(this)
    }


    override fun getReportsData(id : Boolean, context: Context): List<ReportListItem> {
      return model.getReportsDataForList(id)
    }

    override fun getCurrentUser(): String {
        return model.getCurrentUser()
    }

    override fun deletePresenter(){
        model.deleteListReportPresenter()
    }

    override fun filterBy(address: LatLng?, animale: Animale?, data: Long?, dataTo: Long?, range: Double, reportState: Stato?, context: Context): List<ReportListItem> {
        if((address!= null && range >= 0.0 ) || (animale != null) || (data != null) || (dataTo != null) || (reportState != null))
            return model.filterReportBy(address, animale, data, dataTo, range, reportState,context)
        else
            return emptyList()
    }

    override fun updateData(reportsDataForList: List<ReportListItem>) {
        view.updateView(reportsDataForList)
    }

}