package it.pdm.petlost.presenter


import it.pdm.petlost.model.SignUpModel
import it.pdm.petlost.model.util.RegexUtil
import it.pdm.petlost.interfaces.SignUpInterface

class SignUpPresenter : SignUpInterface.PresenterSignUp, SignUpInterface.onRegistrationPresenterListener {
    private var model: SignUpInterface.ModelSignUp = SignUpModel(this)
    private lateinit var view: SignUpInterface.ViewSignUp

    constructor(view: SignUpInterface.ViewSignUp) {
        this.view = view
    }

    fun checkUserInfoFormat(name: String?, surname: String?, email: String?, pw: String?, emailconf : String, passconf : String): Int
    {
        return when{
            name.isNullOrEmpty() -> 1
            surname.isNullOrEmpty() -> 2
            email.isNullOrEmpty() -> 3
            pw.isNullOrEmpty() -> 4
            email != emailconf -> 5
            pw != passconf -> 6
            !RegexUtil.verifyEmailPattern(email) -> 7
            !RegexUtil.verifyPwPattern(pw) -> 8
            else -> 0
        }
    }
    override fun onSuccess() {
        view.onSignUpSuccess()
    }

    override fun onFailure(risultato: String) {
        when(risultato){
            "ERROR_EMAIL_ALREADY_IN_USE"->view.errorDataHandler(9)
        }
        view.onSignUpFailure(risultato)
    }

    override fun tryToSign(name: String, surname: String, email: String, pw: String, emailconf : String, passconf : String){
        model.SignUpRequest(name, surname, email, pw)

    }
}