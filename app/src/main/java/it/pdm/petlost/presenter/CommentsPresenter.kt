package it.pdm.petlost.presenter

import android.graphics.Bitmap
import it.pdm.petlost.interfaces.CommentsInterface
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.data.CommentListItem

class CommentsPresenter(view : CommentsInterface.ViewInterface) : CommentsInterface.PresenterInterface {

    val view : CommentsInterface.ViewInterface
    val model : CommentsInterface.ModelInterface

    init{
        this.view = view
        model = PetLostManager.setCommentsPresenter(this)
    }

    override fun getCommentsReport(repId: String): List<CommentListItem>? {
        return model.getCommentsReport(repId)
    }


    override fun tryToWriteComment(text: String, reportId : String)  {
         model.writeComment(text, reportId)
    }

    override fun deletePresenter() {
        model.deleteCommentPresenter()
    }

    override fun reloadInfo(commentsReport: List<CommentListItem>?) {
        view.reloadComments(commentsReport)
    }

    override fun removeListener(reportId: String) {
        model.removeListener(reportId)
    }

    override fun assignListener(reportId: String) {
        model.assignListener(reportId)
    }

    override fun commentError(commentChangeErr: Int) {
        view.errorDataHandler(commentChangeErr)
    }

    override fun checkInfo(msg: String?): Int {
        return when {
            msg.isNullOrEmpty() -> 1
            msg.length > 500 -> 2
            else -> 0

        }
    }

    override fun tryToWriteCommentWithImage(reportId: String, text: String?, img: Bitmap?) {
        var err = 0
        if(img != null) {
            if (text != null && text != "")
                model.writeCommentWithImage(reportId, text, img)
            else
                model.writeCommentWithImage(reportId, "", img)
        }
        else
            err = 1
        view.errorDataHandler(err)
    }


}