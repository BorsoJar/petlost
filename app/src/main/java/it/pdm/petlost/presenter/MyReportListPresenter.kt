package it.pdm.petlost.presenter

import android.content.Context
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import it.pdm.petlost.interfaces.MyReportInterface
import it.pdm.petlost.interfaces.ReportListInterface
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.ReportListItem

class MyReportListPresenter(val v :  MyReportInterface.MyViewReport) : MyReportInterface.MyRepPresenter {

    private val model : MyReportInterface.MyModelReport = PetLostManager.setMyRepListPresenter(this)

    override fun getReportsData(id: Boolean): List<ReportListItem> {
       return model.getReportsDataForList(id)
    }


    override fun getCurrentUser(): String {
        return model.getCurrentUser()
    }

    override fun updateData(reportsDataForList: List<ReportListItem>) {
        v.updateView(reportsDataForList)
    }

    override fun deleteMyRepListPresenter(){
        model.deleteMyRepListPresenter()
    }



}