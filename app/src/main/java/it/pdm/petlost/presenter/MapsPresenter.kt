package it.pdm.petlost.presenter


import android.content.Context
import it.pdm.petlost.interfaces.MapsReportInterface
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.data.ReportListItem
import it.pdm.petlost.model.data.ReportsMapsItem

class MapsPresenter(v: MapsReportInterface.ViewReport) : MapsReportInterface.PresenterReport {
    private val view: MapsReportInterface.ViewReport = v
    private val model: MapsReportInterface.ModelReport

    init {
        model = PetLostManager.setMapsPresenter(this)
    }


    override fun getReportsData(id: String, context: Context): ReportListItem? {
        return model.getReportsDataForItem(id, context)
    }

    override fun deletePresenter() {
        model.deleteMapsPresenter()
    }

    override fun updateData(mtbList: MutableList<ReportsMapsItem>) {
        view.updateView(mtbList)
    }


}