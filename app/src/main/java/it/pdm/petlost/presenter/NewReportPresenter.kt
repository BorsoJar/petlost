package it.pdm.petlost.presenter


import android.graphics.Bitmap
import com.google.android.libraries.places.api.model.Place
import it.pdm.petlost.interfaces.NewReportInterface
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.MyLatLng
import it.pdm.petlost.model.data.Report
import java.util.*

class NewReportPresenter(v: NewReportInterface.ViewNewReport) : NewReportInterface.PresenterNewReport{
    private var view: NewReportInterface.ViewNewReport = v
    private val model: NewReportInterface.ModelNewReport

    init {
        view.init()
        model = PetLostManager.setNewReportPresenter(this)
    }

    override fun tryToAddNewReport(report: Report, uuid: UUID) {
       model.addNewReportRequest(report,uuid)
    }

    override fun checkReportInfo(
        data: Calendar,
        desc: String,
        animal: Animale?,
        place: Place?,
        latlng: MyLatLng?
    ): Int {
        val currentDate = Calendar.getInstance().set(Calendar.YEAR-1, Calendar.MONTH, Calendar.DAY_OF_MONTH)
        return when{
            data.timeInMillis > Calendar.getInstance().timeInMillis -> 1
            data.before(currentDate) -> 1
            desc.isEmpty() -> 2
            animal == null -> 3
            place?.latLng == null && latlng == null -> 4
            else -> 0
        }
    }


    override fun onCreationReportSuccess() {
        view.onSuccess()
    }

    override fun onCreationReportFailure() {
        view.onFailure()
    }

    override fun tryToAddNewImage(image: Bitmap, uuid: UUID) {
        model.addNewImage(image,uuid)
    }



}