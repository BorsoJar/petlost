package it.pdm.petlost.presenter

import com.google.firebase.auth.FirebaseUser
import it.pdm.petlost.model.LogInModel
import it.pdm.petlost.model.util.RegexUtil
import it.pdm.petlost.interfaces.LogInInterface

class LogInPresenter: LogInInterface.PresenterLogIn, LogInInterface.onLogInPresenterListener {
    private var view: LogInInterface.ViewLogIn
    private val model : LogInInterface.ModelLogIn = LogInModel(this)

    constructor(view : LogInInterface.ViewLogIn)
    {
        this.view = view
    }

    override fun checkUserInfoFormat(email: String, pw: String): Int {
        var error = 0
        when {
            email.isNullOrEmpty() -> error = 1
            pw.isNullOrEmpty() -> error = 2
            !RegexUtil.verifyEmailPattern(email) -> error = 3
            !RegexUtil.verifyPwPattern(pw) -> error = 5
        }
        return error
    }


    override fun tryToLog(email: String, pw: String) {
        model.logInRequest(email, pw)
    }

    override fun tryToResetPassword(email: String) {
        model.resetPasswordRequest(email)
    }

    override fun onLogInSuccess(user: FirebaseUser) {
        view.onLogInSuccess(user)
    }

    override fun onLogInFail(errorCode: String) {
        var err: Int = 0
        when(errorCode)
        {
            "ERROR_INVALID_CREDENTIAL", "ERROR_INVALID_USER_TOKEN", "ERROR_USER_DISABLED", "ERROR_USER_MISMATCH" -> err = 5
            "ERROR_INVALID_EMAIL" -> err = 1
            "ERROR_WRONG_PASSWORD" -> err = 2
            "ERROR_USER_NOT_FOUND" -> err = 4
            "EMAIL_NOT_VERIFIED" -> err = 6
        }
        view.onLogInFail(err)
    }

    fun tryToSendNewMailConfirmation()
    {
        model.sendMailConfirmationAgain()
    }
}