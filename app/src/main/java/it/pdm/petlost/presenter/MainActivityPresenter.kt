package it.pdm.petlost.presenter

import android.content.Context
import android.graphics.Bitmap
import it.pdm.petlost.interfaces.MainActivityInterface
import it.pdm.petlost.model.PetLostManager

class MainActivityPresenter(val view : MainActivityInterface.MainActivityView) : MainActivityInterface.MainActivityPresenter {
    private var model : MainActivityInterface.MainActivityModel


   init {
       view.initView()
       model = PetLostManager.setMainActivityPresenter(this)
   }

    override fun tryToGetUserInfo() {
        model.requestUserInfo()
    }

    override fun tryToLogout(context: Context) {
        model.requestLogout(context)
    }


    override fun deletePresenter() {
        model.deleteMainActivityPresenter()
    }


    override fun onSuccess(requestCode: MainActivityInterface.ActionCode, value: Any?) {
        view.onSuccess(requestCode, null)
    }

    override fun onFailure(requestCode: MainActivityInterface.ActionCode) {
       view.onFailure(requestCode)
    }

    override fun getName(): String {
        return model.getUserReference().name
    }

    override fun getSurname(): String {
        return model.getUserReference().surname
    }

    override fun getPhone(): String {
        TODO("Not yet implemented")
    }

    override fun getPhoto(): Bitmap {
        return model.getUserPhoto()!!
    }

    override fun getEmail(): String {
        return model.getUserReference().email
    }

    override fun isAdmin(): Boolean {
        return model.getUserReference().isAdmin
    }

    override fun hasPhoto(): Boolean {
        TODO("Not yet implemented")
    }


}