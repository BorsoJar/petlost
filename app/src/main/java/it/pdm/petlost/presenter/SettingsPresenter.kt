package it.pdm.petlost.presenter

import android.content.Context
import android.graphics.Bitmap
import it.pdm.petlost.interfaces.SettingsInterface
import it.pdm.petlost.model.PetLostManager

class SettingsPresenter(val view: SettingsInterface.View): SettingsInterface.Presenter{
    private lateinit var model : SettingsInterface.Model

    init {
        model = PetLostManager.setSettingsPresenter(this)
        view.initView()
    }

    override fun tryToSendAdminRequest(request: String, context: Context): SettingsInterface.ErrorCode {
        return model.sendAdminRequest(request, context)
    }

    override fun notificationsActivationRequest(
        context: Context,
        enabled: Boolean
    ) {
        model.notificationsActivation(context, enabled)
    }


    override fun getName(): String {
        TODO("Not yet implemented")
    }

    override fun getSurname(): String {
        TODO("Not yet implemented")
    }

    override fun getPhone(): String {
        TODO("Not yet implemented")
    }

    override fun getPhoto(): Bitmap {
        TODO("Not yet implemented")
    }

    override fun getEmail(): String {
        TODO("Not yet implemented")
    }

    override fun isAdmin(): Boolean {
        return model.getUserReference().isAdmin
    }

    override fun hasPhoto(): Boolean {
        TODO("Not yet implemented")
    }

}