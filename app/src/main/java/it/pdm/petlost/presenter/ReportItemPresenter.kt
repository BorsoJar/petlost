package it.pdm.petlost.presenter

import android.graphics.Bitmap
import it.pdm.petlost.interfaces.NotificationsInterface
import it.pdm.petlost.interfaces.ReportListInterface
import it.pdm.petlost.model.PetLostManager
import it.pdm.petlost.model.data.Stato

class ReportItemPresenter(val view : ReportListInterface.ViewReportItem) : ReportListInterface.PresenterReportItem {

    val model = PetLostManager.setReportItemPresenter(this)

    override fun deleteReportRequest(author: String) {
        model.deleteReport(author)
    }

    override fun deleteReportItemPresenter() {
        model.deleteReportItemPresenter()
    }

    override fun onDeleteSuccess() {
        view.onDeleteSuccess()
    }

    override fun onDeleteFailed() {
        view.onDeleteFailed()
    }

    override fun checkAdmin() : Boolean {
        return model.checkAdmin()
    }

    override fun updateReportState(reportId: String, state: Stato) {
        model.updateReportState(reportId, state)
    }

    override fun tryToGetReportImage(reportID: String) {
        model.downloadReportImageForNotificationRequest(reportID, NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_PRESENTER)
    }

    override fun onSuccess(
        action: NotificationsInterface.Action,
        obj: Any?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when(action) {
            NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_PRESENTER ->view.onSuccess(
                NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_PRESENTER,
                obj as Bitmap,
                null
            )
        }
    }

    override fun onFail(
        error: NotificationsInterface.Error,
        extraInfo: NotificationsInterface.Action?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        view.onFail(error, null, null)
    }
}