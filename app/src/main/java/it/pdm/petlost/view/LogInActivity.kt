package it.pdm.petlost.view

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseUser
import it.pdm.petlost.R
import it.pdm.petlost.presenter.LogInPresenter
import it.pdm.petlost.interfaces.LogInInterface
import it.pdm.petlost.model.util.PasswordUtil
import kotlinx.android.synthetic.main.login_layout.*


class LogInActivity : AppCompatActivity(), LogInInterface.ViewLogIn {
    private var presenter : LogInPresenter? = null
    private var sp : SharedPreferences? = null
    private var email : String? = null
    private var pw: String? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)

        presenter = LogInPresenter(this)
        if(isLogged()){
            try {
                presenter!!.tryToLog(email!!, pw!!)
            }
            finally {
                finish()
            }
        }
//        TEST()
        login_btnLogin.setOnClickListener {
            clearErrMsg()
            val err = presenter!!.checkUserInfoFormat(login_fieldEmail.text.toString(), login_fieldPassword.text.toString())
            if(errorDataHandler(err)) {
                email = login_fieldEmail.text.toString()
                pw = login_fieldPassword.text.toString()
                presenter!!.tryToLog(email!!, pw!!)
            }

        }

        signup_button.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        login_btnResetPassword.setOnClickListener {

            val err = presenter!!.checkUserInfoFormat(login_fieldEmail.text.toString(), "Password1")
            lateinit var message: String
            var isEmailValid = false
            val builder = AlertDialog.Builder(this)
            if(err == 1 || err == 3)
                message = getString(R.string.passwordLostInstructions)
            else {
                isEmailValid = true
                message = getString(R.string.passwordLostMessage) + getString(R.string.passwordLostConfirm) + "${login_fieldEmail.text.toString()} ?"
            }
            builder.setTitle(getString(R.string.passwordLost))
            builder.setMessage(message)
            builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                if (isEmailValid)
                    presenter!!.tryToResetPassword(login_fieldEmail.text.toString())
            }
            if (isEmailValid)
                builder.setNegativeButton("no", { dialog, which ->
                    //operazione annullata
                })
            builder.show()
        }
    }

    override fun onLogInSuccess(user: FirebaseUser) {
        try {
            val editor : SharedPreferences.Editor? = applicationContext.getSharedPreferences(getString(R.string.sharedPrefences), MODE_PRIVATE).edit()
            if(editor!= null){
                editor.putString(getString(R.string.login_sharedPreferencesCredentials_email), email)
                editor.putString(getString(R.string.login_sharedPreferencesCredentials_password), pw)
                editor.apply()
            }
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("user", user)
            startActivity(intent)
        }
        finally {
            finish() 
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onLogInFail(err: Int) {
        clearErrMsg()
        //clearTextView()

        if(err == 6)//user's email isn't verified
            dialogEmailNotVerified().show()
        else
            errorDataHandler(err)
    }

    private fun dialogEmailNotVerified(): AlertDialog.Builder
    {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.emailConfirmation_loginDialog_title))
        builder.setMessage(getString(R.string.emailConfirmation_loginDialog_message))
        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
//            clearTextView()
        }
        builder.setNegativeButton(R.string.emailConfirmation_loginDialog_btnSendAgain){ dialog, which ->
            presenter!!.tryToSendNewMailConfirmation()
        }
        return builder
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun errorDataHandler(err : Int) : Boolean {
        login_layoutEmail.setErrorTextColor(ColorStateList.valueOf(getColor(R.color.light_blue)))
        when(err){
            1 -> login_layoutEmail.error = getString(R.string.accessError_emptyField_email)
            2 -> login_layoutPassword.error = getString(R.string.accessError_emptyField_psw)
            3 -> login_layoutEmail.error = getString(R.string.accessError_notMatching_email)
            4 -> login_layoutEmail.error = getString(R.string.accessError_userNotFound)
            5 -> login_btnLogin.error = getString(R.string.accessError_generic)
            else -> return true
        }
        return false
    }

    private fun clearErrMsg() {
        login_layoutEmail.error = null
        login_layoutPassword.error = null
    }
    private fun clearTextView()
    {
        login_fieldEmail.setText("")
        login_fieldPassword.setText("")
    }

    private fun TEST()
    {
//        ADMIN
//        login_fieldEmail.setText("sgfexjewvtkzmtpkis@wqcefp.com")

//        USER
//        login_fieldEmail.setText("gmzotmdzflojcljsvw@wqcefp.com")

        login_fieldPassword.setText("Qwerty1!")
    }

    private fun isLogged() : Boolean{
        sp = applicationContext.getSharedPreferences(getString(R.string.sharedPrefences), MODE_PRIVATE)
        if(sp!=null){
            email = sp!!.getString(getString(R.string.login_sharedPreferencesCredentials_email), null)
            pw = sp!!.getString(getString(R.string.login_sharedPreferencesCredentials_password), null)
            if(email != null && sp != null)
                return true
        }
            return false
    }
}

