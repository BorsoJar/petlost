package it.pdm.petlost.view

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.annotation.RequiresApi
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import it.pdm.petlost.BuildConfig
import it.pdm.petlost.R
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.Stato
import kotlinx.android.synthetic.main.activity_filter_result.*
import java.text.SimpleDateFormat
import java.util.*

class FilterActivityResult : Activity(), AdapterView.OnItemSelectedListener{

    private var animal: Animale?
    private var dataTo: Date?
    private var address: LatLng?
    private var range: Double
    private var animArray: Array<String>
    private var date1: Calendar?
    private var date2: Calendar?
    private var calendar: Calendar
    private var place: Place? = null
    private var repState : Stato? = null
    private val AUTOCOMPLETE_REQUEST_CODE = 1
    private val minDate = 631148400000
    private var distanceSelect = "m"

    init {
        animal = null
        dataTo = null
        address = null
        range = 0.0
        animArray = arrayOf()
        date1 = null
        date2 = null
        calendar = Calendar.getInstance()
    }


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_result)
        val animalSpinner: Spinner = findViewById(R.id.filter_spinner)
        ArrayAdapter.createFromResource(
            baseContext,
            R.array.animals,
            R.layout.support_simple_spinner_dropdown_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            animalSpinner.adapter = adapter
            animalSpinner.onItemSelectedListener = this
        }

        val distanceSpinner: Spinner = findViewById(R.id.distance_spinner)
        ArrayAdapter.createFromResource(
            baseContext,
            R.array.distance,
            R.layout.support_simple_spinner_dropdown_item
        ).also { arrayAdapter ->
            arrayAdapter.setDropDownViewResource(
                R.layout.support_simple_spinner_dropdown_item
            )
            distanceSpinner.adapter = arrayAdapter
            distanceSpinner.onItemSelectedListener = this

        }


        date1_edit_filter.setText(getString(R.string.filter_date1_hint))
        date2_edit_filter.setText(getString(R.string.filter_date2_hint))

        date1_edit_filter.setOnClickListener {
            if (date1 == null)
                date1 = Calendar.getInstance()
            val listener1 = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                date1!!.set(Calendar.YEAR, year)
                date1!!.set(Calendar.MONTH, month)
                date1!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                date1_edit_filter.setText(
                    SimpleDateFormat(
                        "dd-MM-yyyy",
                        Locale.US
                    ).format(date1!!.time).toString()
                )
            }
            DatePickerDialog(
                this,
                listener1,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        date2_edit_filter.setOnClickListener {
            if (date2 == null)
                date2 = Calendar.getInstance()
            val listener2 = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                date2!!.set(Calendar.YEAR, year)
                date2!!.set(Calendar.MONTH, month)
                date2!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                date2_edit_filter.setText(
                    SimpleDateFormat(
                        "dd-MM-yyyy",
                        Locale.US
                    ).format(date2!!.time).toString()
                )
            }
            DatePickerDialog(
                this,
                listener2,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        address_edit_txt.setText(getString(R.string.filter_addr_hint))
        address_edit_txt.setOnClickListener {
            if (!Places.isInitialized())
                Places.initialize(applicationContext, BuildConfig.MAPS_API_KEY)

                // Set the fields to specify which types of place data to return.
                val fields: List<Place.Field> =
                    listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
                // Start the autocomplete intent.
                val intent = Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.OVERLAY, fields
                )
                    .build(this)
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
            }

        filter_radio_group.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.radio_open -> repState = Stato.APERTA
                R.id.radio_progress -> repState = Stato.PRESA_IN_CARICO
                R.id.radio_closed -> repState = Stato.CHIUSA
            }
        }

        apply_filter_btn.setOnClickListener {
            val intent = Intent()
            if(checkData()) {
                intent.putExtra("data1", date1?.timeInMillis)
                intent.putExtra("data2", date2?.timeInMillis)   //select final date err
                intent.putExtra("animal", animal.toString())    //"null"
                intent.putExtra("lat", address?.latitude)
                intent.putExtra("lon", address?.longitude)
                intent.putExtra("state", repState.toString())
                intent.putExtra("range", range)
                setResult(RESULT_OK, intent)
            }
            else
                setResult(RESULT_CANCELED)
            finish()
        }

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent != null && parent.id == R.id.filter_spinner) {
            val animalStr = parent.getItemAtPosition(position) as String
            when (animalStr) {
                resources.getStringArray(R.array.animals)[1] -> animal = Animale.CANE
                resources.getStringArray(R.array.animals)[2] -> animal = Animale.GATTO
                resources.getStringArray(R.array.animals)[3] -> animal = Animale.TARTARUGA
                resources.getStringArray(R.array.animals)[4] -> animal = Animale.CONIGLIO
                resources.getStringArray(R.array.animals)[5] -> animal = Animale.PAPPAGALLO
                resources.getStringArray(R.array.animals)[6] -> animal = Animale.CRICETO
                resources.getStringArray(R.array.animals)[7] -> animal = Animale.CANARINO
                resources.getStringArray(R.array.animals)[8] -> animal = Animale.SERPENTE
                resources.getStringArray(R.array.animals)[9] -> animal = Animale.RAGNO
                resources.getStringArray(R.array.animals)[10] -> animal = Animale.GALLINA
                else -> null
            }
        }

        else if(parent!=null && parent.id == R.id.distance_spinner && range_filter_edit.text != null){
            when(parent.getItemAtPosition(position)){
                "m" -> distanceSelect = "m"
                "km" -> distanceSelect = "km"
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        animal = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                place = Autocomplete.getPlaceFromIntent(data!!)
                address_edit_txt.setText(place!!.name)
                address = place!!.latLng
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                val status: Status = Autocomplete.getStatusFromIntent(data!!)
                Log.e("FilterActivity", status.toString())
            } else if (resultCode == RESULT_CANCELED) {
                place = null
            }
        }


    }

    private fun checkData() : Boolean {
        var bool = false
        var validaddr = false
        if(date1 != null && date2!= null && date1!!.timeInMillis > minDate && date2!!.timeInMillis > minDate)
            bool = true
        if(animal!= null)
            bool = true
        if(address != null && address!!.latitude>0 && address!!.longitude>0) {
            bool = true
            validaddr = true
        }
        if(repState != null)
            bool = true

        if(!range_filter_edit.text.isNullOrEmpty() && (range_filter_edit.text.toString().toDouble()) > 0.0 && validaddr) {
            val tmprng = range_filter_edit.text.toString().toDouble()
            when (distanceSelect) {
                "m" -> range = 0 + (0.0000073 * tmprng)
                "km" -> range = 0 + (0.0073 * tmprng)
            }
        }
        return bool
    }


}