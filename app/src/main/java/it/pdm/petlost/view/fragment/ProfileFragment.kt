package it.pdm.petlost.view.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.ProfileInterfaces
import it.pdm.petlost.presenter.ProfilePresenter
import it.pdm.petlost.view.MainActivity
import kotlinx.android.synthetic.main.profile_fragment.*
import kotlinx.android.synthetic.main.report_list_fragment.*

class ProfileFragment : Fragment(R.layout.profile_fragment), ProfileInterfaces.View {

    private lateinit var presenter: ProfileInterfaces.Presenter
    val PICK_IMAGE_REQUEST: Int =  1
    private lateinit var mActivity : FragmentActivity


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.let { mActivity = it }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
        presenter = ProfilePresenter(this)
        init()


    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setUserDataFieldEnable(value: Boolean)
    {
        if(value) {
            profile_fieldName.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
            profile_fieldSurname.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
            profile_fieldPhone.inputType = InputType.TYPE_CLASS_PHONE
            profile_picture.isActivated = true
            Toast.makeText(requireContext(), getString(R.string.profile_editEnabled), Toast.LENGTH_SHORT).show()
        }else{
            profile_fieldName.inputType = InputType.TYPE_NULL
            profile_fieldSurname.inputType = InputType.TYPE_NULL
            profile_fieldPhone.inputType = InputType.TYPE_NULL
            profile_picture.isActivated = false
            Toast.makeText(requireContext(), getString(R.string.profile_editDisabled), Toast.LENGTH_SHORT).show()
        }
    }
    private fun setPasswordFieldVisibility(value : Boolean) {
        if(value)
        {
            profile_fieldNewPassword.visibility = View.VISIBLE
            profile_fieldOldPassword.visibility = View.VISIBLE
            profile_fieldNewPasswordConfirmation.visibility = View.VISIBLE
            profile_btnSubmitPasswordUpdate.visibility = View.VISIBLE
        }else {
            profile_fieldNewPassword.visibility = View.GONE
            profile_fieldOldPassword.visibility = View.GONE
            profile_fieldNewPasswordConfirmation.visibility = View.GONE
            profile_btnSubmitPasswordUpdate.visibility = View.GONE
        }
    }

    private fun clearPasswordFields()
    {
        profile_fieldNewPassword.setText("")
        profile_fieldOldPassword.setText("")
        profile_fieldNewPasswordConfirmation.setText("")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun init() {
        initializeField()

        setPasswordFieldVisibility(false)
        profile_btnSubmitChanges.text = getString(R.string.profile_editData)
        setUserDataFieldEnable(false)

        profile_picture.setOnClickListener {
            if (profile_picture.isActivated) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                        Intent.createChooser(intent, "Select Picture"),
                        PICK_IMAGE_REQUEST
                )
            }
        }

        profile_fieldEmail.setOnClickListener {
            Toast.makeText(requireContext(), getString(R.string.profile_cantEditEmailField), Toast.LENGTH_SHORT).show()
        }
        profile_btnChangePassword.setOnClickListener(View.OnClickListener {
            setPasswordFieldVisibility(!profile_fieldOldPassword.isVisible)

        })

        profile_btnSubmitPasswordUpdate.setOnClickListener {

            val err = presenter.checkPassword(  profile_fieldOldPassword.text.toString(),
                    profile_fieldNewPassword.text.toString(),
                    profile_fieldNewPasswordConfirmation.text.toString())
            if(err == 0) {
                Log.d("ProfileFragment", "inizio procedura cambio psw")
                presenter.tryToChangePassword(profile_fieldOldPassword.text.toString(), profile_fieldNewPassword.text.toString())
            }
            else
                handleErrorPasswordChange(err)
        }

        profile_btnSubmitChanges.setOnClickListener(View.OnClickListener {



            if(profile_btnSubmitChanges.text == getString(R.string.profile_editData)) {
                profile_btnSubmitChanges.text = getString(R.string.profile_confirmUserDataChanging)
                setUserDataFieldEnable(true)
            }
            else {
                profile_btnSubmitChanges.text = getString(R.string.profile_editData)
                setUserDataFieldEnable(false)



//            ris è un array booleano di 4 celle.
//            [0] == true significa che il nome è cambiato
//            [1] == true significa che il cognome è cambiato
//            [2] == true significa che il phone è cambiato
//            [3] == true significa che la foto è cambiata
                val ris = presenter.whichDataHasChanged(profile_fieldName.text.toString(), profile_fieldSurname.text.toString(), profile_fieldPhone.text.toString(), (profile_picture.drawable).current.toBitmap())

                if(ris[0])
                    presenter.tryToChangeName(profile_fieldName.text.toString())
                if(ris[1])
                    presenter.tryToChangeSurname(profile_fieldSurname.text.toString())
                if(ris[2])
                    presenter.tryToChangePhone(profile_fieldPhone.text.toString())
                if(ris[3])
                    presenter.tryToChangePhoto((profile_picture.drawable).current.toBitmap())
            }
        })

    }

    override fun handleErrorPasswordChange(errorCode: Int) {
        when(errorCode)
        {
            1 -> Log.e("ProfileFragment", "oldPassword field is null or empty")
            2 -> Log.e("ProfileFragment", "newPassword field is null or empty")
            3 -> Log.e("ProfileFragment", "newPasswordConfirmation field is null or empty")
            4 -> Log.e("ProfileFragment", "newPassword and newPasswordConfirmation field doesn't match")
            5 -> Log.e("ProfileFragment", "newPassword format is invalid")
            6 -> Log.e("ProfileFragment", "Authentication Error. Old password could be wrong")
            7-> Log.e("ProfileFragment", "Generic error. Please try again later")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        lateinit var bitmap: Bitmap
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK)
        {
            if (data == null || data.data == null) {
                return
            }
            val filePath = data.data
            val selectedPhotoUri = data.data
            try {
                selectedPhotoUri?.let {
                    if (Build.VERSION.SDK_INT < 28) {
                        bitmap = MediaStore.Images.Media.getBitmap(
                                requireActivity().contentResolver,
                                selectedPhotoUri
                        )
                    } else {
                        val source = ImageDecoder.createSource(requireActivity().contentResolver, selectedPhotoUri)
                        bitmap = ImageDecoder.decodeBitmap(source)
                    }
                }
                if (bitmap != null) {
                    profile_picture.setImageBitmap(bitmap)
                }
            } catch (e: Exception) {
                Log.e("ProfileFragment", "Errore nel caricamento della foto, ${e.toString()}")
            }
        }
    }

    private fun initializeField()
    {
        profile_fieldName.setText(presenter.getName())
        profile_fieldSurname.setText(presenter.getSurname())
        profile_fieldPhone.setText(presenter.getPhone())
        profile_fieldEmail.setText(presenter.getEmail())

        if(presenter.hasPhoto())
            profile_picture.setImageBitmap(presenter.getPhoto())
        else
            profile_picture.setImageResource(R.drawable.ic_baseline_camera_alt_24)


    }

    private fun onNameChangingSuccess() {
        Log.d("ProfileFragment", "User name update correctly")
    }

    private fun onNameChangingFailure(errorMessage: ProfileInterfaces.ErrorCode) {
        Log.e("ProfileFragment", "Error updating user name: $errorMessage")
    }

    private fun onSurnameChangingSuccess() {
        Log.d("ProfileFragment", "User surname update correctly")
    }

    private fun onSurnameChangingFailure(errorMessage: ProfileInterfaces.ErrorCode) {
        Log.e("ProfileFragment", "Error updating user surname: $errorMessage")
    }

    private fun onPhotoChangingSuccess() {
        Log.d("ProfileFragment", "User photo update correctly")
    }

    private fun onPhotoChangingFailure(errorMessage: ProfileInterfaces.ErrorCode) {
        Log.e("ProfileFragment", "Error updating user photo: $errorMessage")
    }

    private fun onPhoneChangingSuccess() {
        Log.d("ProfileFragment", "User phone update correctly")
    }

    private fun onPhoneChangingFailure(errorMessage: ProfileInterfaces.ErrorCode) {
        Log.e("ProfileFragment", "Error updating user phone: $errorMessage")
    }

    private fun onPasswordChangingSuccess() {
        Log.d("ProfileFragment", "User password update correctly")
        clearPasswordFields()
        setPasswordFieldVisibility(!profile_fieldOldPassword.isVisible)

    }

    private fun onPasswordChangingFailure(errorMessage: ProfileInterfaces.ErrorCode) {

        //todo mostrare gli erroi su un toast
        val errCode = when(errorMessage)
        {
            ProfileInterfaces.ErrorCode.AUTH_ERROR -> 6
            ProfileInterfaces.ErrorCode.GENERIC_ERROR -> 7
        }
        handleErrorPasswordChange(errCode)
    }

    override fun onSuccess(requestCode: ProfileInterfaces.ActionCode) {
        when(requestCode)
        {
            ProfileInterfaces.ActionCode.NAME_UPDATE -> onNameChangingSuccess()
            ProfileInterfaces.ActionCode.SURNAME_UPDATE -> onSurnameChangingSuccess()
            ProfileInterfaces.ActionCode.PHONE_UPDATE -> onPhoneChangingSuccess()
            ProfileInterfaces.ActionCode.PHOTO_UPDATE -> onPhotoChangingSuccess()
            ProfileInterfaces.ActionCode.PASSWORD_UPDATE -> onPasswordChangingSuccess()
        }
    }

    override fun onFailure(requestCode: ProfileInterfaces.ActionCode, errorMessage: ProfileInterfaces.ErrorCode) {
        when(requestCode)
        {
            ProfileInterfaces.ActionCode.NAME_UPDATE -> onNameChangingFailure(errorMessage)
            ProfileInterfaces.ActionCode.SURNAME_UPDATE -> onSurnameChangingFailure(errorMessage)
            ProfileInterfaces.ActionCode.PHONE_UPDATE -> onPhoneChangingFailure(errorMessage)
            ProfileInterfaces.ActionCode.PHOTO_UPDATE -> onPhotoChangingFailure(errorMessage)
            ProfileInterfaces.ActionCode.PASSWORD_UPDATE -> onPasswordChangingFailure(errorMessage)
        }
    }

    private fun setUpToolbar() {

        val mainActivity = mActivity as MainActivity
        val navigationView: NavigationView = mActivity.findViewById(R.id.nav_view)

        mainActivity.setSupportActionBar(toolbar_profile)
        val navController = NavHostFragment.findNavController(this)
        val appBarConfiguration = mainActivity.appBarConfiguration
        NavigationUI.setupActionBarWithNavController(
            mainActivity,
            navController,
            appBarConfiguration
        )
        NavigationUI.setupWithNavController(navigationView,navController)

    }

}