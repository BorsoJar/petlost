package it.pdm.petlost.view.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.navigation.NavigationView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.Adapter
import it.pdm.petlost.interfaces.ReportListInterface
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.MyLatLng
import it.pdm.petlost.model.data.ReportListItem
import it.pdm.petlost.model.data.Stato
import it.pdm.petlost.model.util.ReportListAdapter
import it.pdm.petlost.presenter.ReportListPresenter
import it.pdm.petlost.view.FilterActivityResult
import it.pdm.petlost.view.MainActivity
import kotlinx.android.synthetic.main.report_list_fragment.*

class ReportListFragment : Fragment(R.layout.report_list_fragment), ReportListInterface.ViewReportList, Adapter.ItemClickInterface {

    private val presenter : ReportListInterface.PresenterReportList = ReportListPresenter(this)
    private lateinit var list : MutableList<ReportListItem>
    private lateinit var mActivity : FragmentActivity
    private var filter : Boolean = false
    private var layoutManager : LinearLayoutManager? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.let { mActivity = it }
    }


    override fun onViewCreated(view : View, savedInstanceState: Bundle?) {
        if(savedInstanceState == null)
            list = presenter.getReportsData(false, requireContext()) as MutableList<ReportListItem>

        super.onCreate(savedInstanceState)
        setUpToolbar()
        layoutManager = LinearLayoutManager(requireContext())
        layoutManager!!.stackFromEnd = true

        reloadView()


        filter_button.setOnClickListener {
            if (filter) {
                list =  presenter.getReportsData(false, requireContext()) as MutableList<ReportListItem>
                reloadView()
                filter_button.setImageResource(
                    R.drawable.ic_baseline_filter_alt_24
                )
                filter = false
            }
            else {
                val intent = Intent(context, FilterActivityResult::class.java)
                startActivityForResult(intent, 0)

            }
        }

    }


    override fun onItemClick(position: Int) {
        var user = false
        if(list[position].authorId.equals(presenter.getCurrentUser()))
            user = true
        val date = list[position].retriveDate(list[position].data)
        val animal = list[position].anType
        val state = list[position].state
        val img = list[position].img
        val desc = list[position].desc
        val place =list[position].place
        val reportid = list[position].reportId


       val action = ReportListFragmentDirections.actionReportListFragmentToItemListFragment(date,animal,state,desc,img,place,user,reportid)
        requireView().findNavController().navigate(action)
        presenter.deletePresenter()

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.deletePresenter()

    }


    private fun reloadView(){

        for(item in list)
            item.place = placeTranslater(item,requireContext())

            recycle_list_report.adapter = ReportListAdapter(list, this,requireContext())
            recycle_list_report.layoutManager = layoutManager
            recycle_list_report.setHasFixedSize(true)

         if(list.isNullOrEmpty())
            Toast.makeText(requireContext(),"No report founded", Toast.LENGTH_LONG).show()

    }

    override fun updateView(reportsDataForList: List<ReportListItem>) {
        list = reportsDataForList.toMutableList()
        reloadView()
    }

    private fun setUpToolbar() {

        val mainActivity = mActivity as MainActivity
        val navigationView: NavigationView = mActivity.findViewById(R.id.nav_view)

        mainActivity.setSupportActionBar(toolbar_list_rep)
        val navController = NavHostFragment.findNavController(this)
        val appBarConfiguration = mainActivity.appBarConfiguration
        NavigationUI.setupActionBarWithNavController(
            mainActivity,
            navController,
            appBarConfiguration
        )
        NavigationUI.setupWithNavController(navigationView,navController)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            filter = true
            filter_button.setImageBitmap(
                BitmapFactory.decodeResource(
                    resources,
                    R.drawable.ic_remove_filter
                )
            )
            if(data != null) {
                var animal : Animale? = null
                var data1 : Long? = data.getLongExtra("data1",-1)
                var data2 : Long? = data.getLongExtra("data2",-1)
                val anim = data.getStringExtra("animal")
                val lat = data.getDoubleExtra("lat",0.0)
                val lon = data.getDoubleExtra("lon", 0.0)
                var address : LatLng? = null
                val statetmp : String? = data.getStringExtra("state")
                val range = data.getDoubleExtra("range", 0.0)
                var reportState : Stato? = Stato.APERTA
                if(data1!= null && data1 < 0 && data2 != null && data2 < 0){
                    data1 = null
                    data2 = null
                }

                when (anim) {
                    "CANE" -> animal = Animale.CANE
                    "GATTO" -> animal = Animale.GATTO
                    "TARTARUGA" -> animal = Animale.TARTARUGA
                    "CONIGLIO" -> animal = Animale.CONIGLIO
                    "PAPPAGALLO" -> animal = Animale.PAPPAGALLO
                    "CRICETO" -> animal = Animale.CRICETO
                    "CANARINO" -> animal = Animale.CANARINO
                    "SERPENTE" -> animal = Animale.SERPENTE
                    "RAGNO" -> animal = Animale.RAGNO
                    "GALLINA" -> animal = Animale.GALLINA
                    else -> animal = null
                }


                if (lat > 0 && lon > 0 && range >= 0) {
                    address = LatLng(lat, lon)
                }
                when(statetmp){
                    "PRESA_IN_CARICO" -> reportState = Stato.PRESA_IN_CARICO
                    "CHIUSA" -> reportState = Stato.CHIUSA
                    else -> reportState = null
                }
                val filterlist =
                    presenter.filterBy(address, animal, data1, data2, range,reportState, requireContext())

                if(filterlist.isNullOrEmpty())
                    list = emptyList<ReportListItem>().toMutableList()
                else
                    list = filterlist.toMutableList()

                reloadView()
                Toast.makeText(requireContext(), "List contiene : "+list.size, Toast.LENGTH_LONG).show()


            }
        }
        else{
            Log.i("ReportListFrag", "Filter canceled")
        }

    }

    private fun placeTranslater(reportListItem: ReportListItem, context: Context) : String{
        var address : String
        var myLatLng = MyLatLng()
        myLatLng = myLatLng.stringToLatLng(reportListItem.place)
        address = myLatLng.convertLatLngToAddress(myLatLng,context)
        return address
    }

}