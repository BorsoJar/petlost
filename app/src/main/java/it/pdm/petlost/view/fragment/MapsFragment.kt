package it.pdm.petlost.view.fragment


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.navigation.NavigationView
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import it.pdm.petlost.BuildConfig
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.Adapter
import it.pdm.petlost.interfaces.MapsReportInterface
import it.pdm.petlost.model.data.ReportClusterItem
import it.pdm.petlost.model.data.ReportsMapsItem
import it.pdm.petlost.presenter.MapsPresenter
import it.pdm.petlost.view.MainActivity
import it.pdm.petlost.view.NewReportActivity
import kotlinx.android.synthetic.main.maps_fragment.*


class MapsFragment : Fragment(R.layout.maps_fragment), OnMapReadyCallback,
    MapsReportInterface.ViewReport, Adapter.InfoWindowInterface {

    private val NEW_REPORTING_REQUEST_CODE = 1
    private lateinit var presenter: MapsReportInterface.PresenterReport
    private lateinit var mActivity : FragmentActivity
    private val AUTOCOMPLETE_REQUEST_CODE = 2
    val UPDATE = 3
    private lateinit var clusterManager: ClusterManager<ReportClusterItem>



    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.let { mActivity = it }
    }


    //da capire meglio a cosa serve, in questo caso, passargli savedInstance nella on create di map
    companion object {
        private const val MAPVIEW_BUNDLE_KEY = "MapViewBundleKey"
    }

    private lateinit var mMap: GoogleMap
    private var reportList: MutableList<ReportsMapsItem> = mutableListOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()

        val mapViewBundle = savedInstanceState?.getBundle(MAPVIEW_BUNDLE_KEY)
        map.onCreate(mapViewBundle)
        map.getMapAsync(this)

        new_reporting.setOnClickListener {
            val intent = Intent(activity, NewReportActivity::class.java)
            startActivityForResult(intent, NEW_REPORTING_REQUEST_CODE)
        }

        maps_search_btn.setOnClickListener {
            if (!Places.isInitialized())
                Places.initialize(requireContext(), BuildConfig.MAPS_API_KEY)
            val fields: List<Place.Field> =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields
            )
                .build(requireContext())
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }



    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY) ?: Bundle().also {
            outState.putBundle(MAPVIEW_BUNDLE_KEY, it)
        }
        //       map.onSaveInstanceState(mapViewBundle)
    }

    @SuppressLint("PotentialBehaviorOverride")
    override fun onMapReady(map: GoogleMap) {
        mMap = map
        clusterManager = ClusterManager(context, mMap)
        presenter = MapsPresenter(this)


        setUpClusterer()


    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onStart() {
        super.onStart()
        map.onStart()
    }

    override fun onStop() {
        super.onStop()
        map.onStop()
    }

    override fun onPause() {
        map.onPause()
        super.onPause()
    }

    override fun updateView(mtbList: MutableList<ReportsMapsItem>) {
        clusterManager.clearItems()
        mMap.clear()
        reportList.clear()
        for(report in mtbList)
            reportList.add(report)
        addItems()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    private fun setUpToolbar() {

        val mainActivity = mActivity as MainActivity
        val navigationView: NavigationView = mActivity.findViewById(R.id.nav_view)

        mainActivity.setSupportActionBar(toolbar_maps)
        val navController = NavHostFragment.findNavController(this)
        val appBarConfiguration = mainActivity.appBarConfiguration
        NavigationUI.setupActionBarWithNavController(
            mainActivity,
            navController,
            appBarConfiguration
        )
        NavigationUI.setupWithNavController(navigationView,navController)

    }

    private fun setUpClusterer() {
        mMap.setOnCameraIdleListener(clusterManager)
        mMap.setOnMarkerClickListener(clusterManager)
        clusterManager.setOnClusterItemInfoWindowClickListener {
            val rep = presenter.getReportsData(it.getReportId(), requireContext())
            if (rep != null) {
                val action = MapsFragmentDirections.actionNavMapsToItemListFragment(
                    rep.retriveDate(rep.data),
                    rep.anType,
                    rep.state,
                    rep.desc,
                    rep.img,
                    rep.place,
                    false,
                    rep.reportId
                )
                requireView().findNavController().navigate(action)
            }
        }
    }

    private fun addItems(){
        for(report in reportList){
            val item = ReportClusterItem(report.site.lat, report.site.lng,report.title, report.site.convertLatLngToAddress(report.site,requireContext()),report.repid)
            clusterManager.addItem(item)
        }
        clusterManager.cluster()
        clusterManager.setAnimation(true)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == AUTOCOMPLETE_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            val place = data?.let {
                Autocomplete.getPlaceFromIntent(data)
            }
            if(place != null){
                val cameraPosition = CameraPosition.Builder()
                    .target(place.latLng!!)
                    .zoom(14f)
                    .bearing(90f)
                    .tilt(30f)
                    .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }

        }

    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.deletePresenter()
        clusterManager.clearItems()

    }


}





