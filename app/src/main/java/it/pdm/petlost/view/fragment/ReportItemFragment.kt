package it.pdm.petlost.view.fragment

import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.NotificationsInterface
import it.pdm.petlost.interfaces.ReportListInterface
import it.pdm.petlost.model.data.Stato
import it.pdm.petlost.presenter.ReportItemPresenter
import it.pdm.petlost.view.MainActivity
import kotlinx.android.synthetic.main.report_list_item_fragment.*

class ReportItemFragment : Fragment(R.layout.report_list_item_fragment), ReportListInterface.ViewReportItem {

    private val args: ReportItemFragmentArgs by navArgs()
    private val presenter: ReportListInterface.PresenterReportItem
    private lateinit var mActivity: FragmentActivity


    init {
        presenter = ReportItemPresenter(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity?.let { mActivity = it }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var report = args.reportId
        date_item.text = args.date
        if (args.img != null)
            segna_image_item.setImageBitmap(args.img)
        else
            presenter.tryToGetReportImage(report)
        if (args.user || checkAdmin())
            del_button.isVisible = args.user
        else
            del_button.isVisible = false
        animal_txt.text = translate(args.animal)
        placeField.text = args.place
        description_edit_text.text = args.desc
        state_txt_item.text = translate(args.state)


        setUpToolbar()


        del_button.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle(resources.getString(R.string.del_title))
            builder.setMessage(resources.getString(R.string.del_msg))
            builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                presenter.deleteReportRequest(report)
                findNavController().popBackStack()
            }
            builder.setNegativeButton(R.string.builde_neg) { dialog, which ->
                dialog.cancel()
            }
            builder.show()
        }

        comment_button.setOnClickListener { view ->

            val action =
                ReportItemFragmentDirections.actionReportItemFragmentToCommentsFragment(report)
            view.findNavController().navigate(action)

        }

        state_txt_item.setOnClickListener {
            if (true) {
                var state: Stato
                val builder = AlertDialog.Builder(mActivity)
                val builder2 = AlertDialog.Builder(mActivity)
                builder.setTitle(R.string.state_title)
                builder.setItems(
                    R.array.state
                ) { dialog, which ->
                    when (which) {
                        0 -> {
                            state = Stato.APERTA
                            builder2.setTitle(R.string.build_conf_msg)
                            builder2.setPositiveButton(android.R.string.ok) { dialog, which ->
                                if (checkReportState(state,toState(args.state))) {
                                    presenter.updateReportState(args.reportId, state)
                                    findNavController().popBackStack()
                                }

                            }
                            builder2.setNegativeButton(R.string.builde_neg) { dialog, which ->
                                dialog.cancel()
                            }
                            builder2.create().show()
                        }

                        1 -> {
                            state = Stato.PRESA_IN_CARICO
                            builder2.setTitle(R.string.build_conf_msg)
                            builder2.setPositiveButton(android.R.string.ok) { dialog, which ->
                                if (checkReportState(state,toState(args.state))) {
                                    presenter.updateReportState(args.reportId, state)
                                    findNavController().popBackStack()
                                }
                            }
                            builder2.setNegativeButton(R.string.builde_neg) { dialog, which ->
                                dialog.cancel()
                            }
                            builder2.create().show()
                        }
                        2 -> {
                            state = Stato.CHIUSA
                            builder2.setTitle(R.string.build_conf_msg)
                            builder2.setPositiveButton(android.R.string.ok) { dialog, which ->
                                if (checkReportState(state,toState(args.state))) {
                                    presenter.updateReportState(args.reportId, state)
                                    findNavController().popBackStack()
                                }
                            }
                            builder2.setNegativeButton(R.string.builde_neg) { dialog, which ->
                                dialog.cancel()
                            }
                            builder2.create().show()
                        }
                    }
                }

                builder.setNegativeButton(R.string.builde_neg) { dialog, which ->
                    dialog.cancel()
                }
                builder.create().show()
            }
        }
    }

    override fun onDeleteSuccess() {
        Toast.makeText(requireContext(), "Delete Success!", Toast.LENGTH_LONG).show()
    }

    override fun onDeleteFailed() {
        Toast.makeText(requireContext(), "Delete Failed!", Toast.LENGTH_LONG).show()
    }

    private fun setUpToolbar() {

        val mainActivity = mActivity as MainActivity
        val navigationView: NavigationView = mActivity.findViewById(R.id.nav_view)

        mainActivity.setSupportActionBar(toolbar_item_rep)
        val navController = NavHostFragment.findNavController(this)
        NavigationUI.setupActionBarWithNavController(mainActivity, navController)
        NavigationUI.setupWithNavController(navigationView, navController)

    }

    override fun onSuccess(
        action: NotificationsInterface.Action,
        obj: Any?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when (action) {
            NotificationsInterface.Action.GET_REPORT_IMAGE_CALLBY_PRESENTER -> segna_image_item.setImageBitmap(
                obj as Bitmap
            )
        }
    }

    override fun onFail(
        error: NotificationsInterface.Error,
        extraInfo: NotificationsInterface.Action?,
        broadcastReceiver: NotificationsInterface.NewReportListener?
    ) {
        when (error) {
            NotificationsInterface.Error.FAILED_DOWNLOAD_REPORT_IMAGE -> Log.e(
                "ReportItemFragment",
                "Errore download report image"
            )
            NotificationsInterface.Error.REPORT_IMAGE_DEOSNT_EXIST -> Log.e(
                "ReportItemFragment",
                "non esiste alcuna report image associata a questa report"
            )
        }
    }

    private fun checkAdmin(): Boolean {

        val prefs = requireContext().getSharedPreferences(getString(R.string.sharedPrefences), Context.MODE_PRIVATE);
        return prefs.getBoolean(getString(R.string.notification_flag_isAdmin), false)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.deleteReportItemPresenter()
    }

    private fun checkReportState(state: Stato, precstate : Stato): Boolean {
        if(state.ordinal < precstate.ordinal) {
            Toast.makeText(
                requireContext(),
                "Impossible to change current state",
                Toast.LENGTH_LONG
            ).show()
            return false
        }
        else
            return true
    }

    private fun toState(s : String) : Stato {
        when (s) {
            "APERTA" -> return Stato.APERTA
            "PRESA_IN_CARICO" -> return Stato.PRESA_IN_CARICO
            "CHIUSA" -> return Stato.CHIUSA
            else -> return Stato.APERTA
        }
    }

    private fun translate(any: String): String {
        val context = requireContext()
        var str: String = ""
        when (any) {
            "CANE" -> str = context.resources.getString(R.string.cane)
            "GATTO" -> str = context.resources.getString(R.string.gatto)
            "TARTARUGA" -> str = context.resources.getString(R.string.tarta)
            "CONIGLIO" -> str = context.resources.getString(R.string.coni)
            "PAPPAGALLO" -> str = context.resources.getString(R.string.papp)
            "CRICETO" -> str = context.resources.getString(R.string.crice)
            "CANARINO" -> str = context.resources.getString(R.string.canar)
            "SERPENTE" -> str = context.resources.getString(R.string.serpe)
            "RAGNO" -> str = context.resources.getString(R.string.ragno)
            "GALLINA" -> str = context.resources.getString(R.string.galli)
            "APERTA" -> str = context.resources.getString(R.string.stato_aperta)
            "PRESA_IN_CARICO" -> str = context.resources.getString(R.string.presa_carico)
            "CHIUSA" -> str = context.resources.getString(R.string.chiusa)
        }
        return str
    }
}