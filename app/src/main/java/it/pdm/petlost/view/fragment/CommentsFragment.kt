package it.pdm.petlost.view.fragment


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.Adapter
import it.pdm.petlost.interfaces.CommentsInterface
import it.pdm.petlost.model.data.CommentListItem
import it.pdm.petlost.model.util.CommentListAdapter
import it.pdm.petlost.presenter.CommentsPresenter
import it.pdm.petlost.view.MainActivity
import kotlinx.android.synthetic.main.comments_fragment.*
import kotlinx.android.synthetic.main.report_list_item_fragment.*

class CommentsFragment : Fragment(R.layout.comments_fragment), CommentsInterface.ViewInterface,
    Adapter.ItemClickInterface {

    private val argument: CommentsFragmentArgs by navArgs()
    lateinit var presenter: CommentsInterface.PresenterInterface
    private var list: ArrayList<CommentListItem>? = null
    private lateinit var repId : String
    private val PICK_IMAGE_REQUEST = 0
    private var filePath: Uri? = null
    private var image: Bitmap? = null
    private lateinit var mActivity : FragmentActivity
    private var layoutManager : LinearLayoutManager? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity?.let { mActivity = it }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
        repId  = argument.repID
        presenter = CommentsPresenter(this)

        presenter.assignListener(repId)

        layoutManager = LinearLayoutManager(requireContext())
        layoutManager!!.stackFromEnd = true
        send_btn.setOnClickListener {
            val msg = mesg_text.text.toString()
            val err = presenter.checkInfo(msg)
            if(image!=null)
                presenter.tryToWriteCommentWithImage(repId,msg,image)
            else if (image == null && errorDataHandler(err))
                presenter.tryToWriteComment(msg, repId)
            image = null
            mesg_text.setText("")
        }

        add_img_btn.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT

            startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE_REQUEST
            )

        }

        reloadLayout()
        list = presenter.getCommentsReport(repId) as ArrayList<CommentListItem>?

    }

    override fun reloadComments(commentsReport: List<CommentListItem>?) {
        if (list == null)
            list = arrayListOf()
        if (commentsReport != null ) {
            list!!.clear()
            list = commentsReport as ArrayList<CommentListItem>?
        }

        reloadLayout()
    }

    override fun errorDataHandler(error: Int): Boolean {
        var b = false
        when (error) {
            -1 -> showMessage("Unable to get comment list")
            1 -> showMessage("Description must be not null")
            2 -> showMessage("Description is too long!")
            else -> b = true
        }
        return b
    }

    override fun onItemClick(position: Int) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val contentResolver = requireContext().contentResolver

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data == null || data.data == null) {
                return
            }
            filePath = data.data
            val selectedPhotoUri = data.data
            try {
                selectedPhotoUri?.let {
                    if (Build.VERSION.SDK_INT < 28) {
                        image = MediaStore.Images.Media.getBitmap(
                            contentResolver,
                            selectedPhotoUri
                        )
                    } else {
                        val source =
                            ImageDecoder.createSource(contentResolver, selectedPhotoUri)
                        image = ImageDecoder.decodeBitmap(source)
                    }
                }
                if (image != null) {
                   showMessage("Photo saved! Click send button for upload the image \nor write a message")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showMessage(s: String) {
        Toast.makeText(requireContext(), s, Toast.LENGTH_LONG).show()
    }

    private fun reloadLayout(){
        if(!list.isNullOrEmpty()) {
            recycle_comment_list_report.adapter = CommentListAdapter(list!!, this)
            recycle_comment_list_report.layoutManager = layoutManager
            recycle_comment_list_report.setHasFixedSize(true)
        }
    }

    override fun onDestroy() {
        presenter.removeListener(repId)
        presenter.deletePresenter()
        super.onDestroy()

    }
    private fun setUpToolbar() {

        val mainActivity = mActivity as MainActivity
        val navigationView: NavigationView = mActivity.findViewById(R.id.nav_view)

        mainActivity.setSupportActionBar(toolbar_comment)
        val navController = NavHostFragment.findNavController(this)
        NavigationUI.setupActionBarWithNavController(mainActivity,navController)
        NavigationUI.setupWithNavController(navigationView,navController)

    }


}