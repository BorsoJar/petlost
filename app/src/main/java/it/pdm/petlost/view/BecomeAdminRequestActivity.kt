package it.pdm.petlost.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import it.pdm.petlost.R
import kotlinx.android.synthetic.main.activity_become_admin_request.*


class BecomeAdminRequestActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_become_admin_request)

        adminRequest_resetButton.setOnClickListener {
            adminRequest_request.setText("")
        }

        adminRequest_submitButton.setOnClickListener {
            var resultCode = 0
            val returnIntent = Intent()
            if(adminRequest_request.text.isNullOrEmpty()) {
                Toast.makeText(this, "your request messagge is still empty. Canceled Operation", Toast.LENGTH_LONG).show()
                resultCode = RESULT_CANCELED
            }
            else{
                resultCode = RESULT_OK
                returnIntent.putExtra("requestMessage",adminRequest_request.text.toString());
            }
            setResult(resultCode, returnIntent)
            finish()
        }


    }


}