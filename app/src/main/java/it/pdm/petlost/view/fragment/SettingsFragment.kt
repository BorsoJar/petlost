package it.pdm.petlost.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.SettingsInterface
import it.pdm.petlost.model.NotificationsService
import it.pdm.petlost.presenter.SettingsPresenter
import it.pdm.petlost.view.BecomeAdminRequestActivity
import it.pdm.petlost.view.MainActivity
import kotlinx.android.synthetic.main.report_list_fragment.*
import kotlinx.android.synthetic.main.settings_fragment.*

class SettingsFragment: Fragment(R.layout.settings_fragment), SettingsInterface.View {
    private lateinit var presenter: SettingsInterface.Presenter
    private val ADMIN_REQUEST_CODE = 1
    private lateinit var mActivity : FragmentActivity


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.let { mActivity = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
        presenter = SettingsPresenter(this)
    }

    override fun initView() {

        settings_adminRequestButton.setOnClickListener { v->
            if(!presenter.isAdmin())
            {
                val intent = Intent(activity, BecomeAdminRequestActivity::class.java)
                startActivityForResult(intent, ADMIN_REQUEST_CODE)
            }else
                Toast.makeText(requireContext(), getString(R.string.adminRequest_alreadyAdmin), Toast.LENGTH_LONG).show()
        }

        val prefs = requireContext().getSharedPreferences(getString(R.string.sharedPrefences), Context.MODE_PRIVATE)
        setting_activationNotificationSwitchButton.isChecked = prefs.getBoolean(getString(R.string.notification_flag_userPreferences), false)

        setting_activationNotificationSwitchButton.setOnCheckedChangeListener{view, isChecked ->
            if(!presenter.isAdmin()) {
                presenter.notificationsActivationRequest(requireContext(), isChecked)
                val intent = Intent(context, NotificationsService::class.java)
                context?.startService(intent)

            }
            else {
                Toast.makeText(requireContext(), getString(R.string.adminRequest_alreadyAdmin), Toast.LENGTH_LONG).show()
                setting_activationNotificationSwitchButton.isChecked = !isChecked
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode) {
            ADMIN_REQUEST_CODE -> {
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    val requestMessage: String = data!!.getStringExtra("requestMessage")!!
                    val res = presenter.tryToSendAdminRequest(requestMessage, requireContext())
                    when (res) {
                        SettingsInterface.ErrorCode.ADMIN_REQUEST_SUCCESS -> Toast.makeText(requireContext(), getString(R.string.adminRequest_success), Toast.LENGTH_LONG).show()
                        SettingsInterface.ErrorCode.ADMIN_REQUEST_FAIL -> Toast.makeText(requireContext(), getString(R.string.adminRequest_fail), Toast.LENGTH_LONG).show()
                    }
                } else
                    Toast.makeText(requireContext(), "operazione annullata", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setUpToolbar() {

        val mainActivity = mActivity as MainActivity
        val navigationView: NavigationView = mActivity.findViewById(R.id.nav_view)

        mainActivity.setSupportActionBar(toolbar_settings)
        val navController = NavHostFragment.findNavController(this)
        val appBarConfiguration = mainActivity.appBarConfiguration
        NavigationUI.setupActionBarWithNavController(
            mainActivity,
            navController,
            appBarConfiguration
        )
        NavigationUI.setupWithNavController(navigationView,navController)
    }
}