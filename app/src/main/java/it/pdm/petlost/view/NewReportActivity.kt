package it.pdm.petlost.view


import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import it.pdm.petlost.BuildConfig
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.NewReportInterface
import it.pdm.petlost.model.data.Animale
import it.pdm.petlost.model.data.MyLatLng
import it.pdm.petlost.model.data.Report
import it.pdm.petlost.model.data.Stato
import it.pdm.petlost.presenter.NewReportPresenter
import kotlinx.android.synthetic.main.new_reporting_layout.*
import java.text.SimpleDateFormat
import java.util.*


class NewReportActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener,
        NewReportInterface.ViewNewReport {

    private lateinit var presenter: NewReportPresenter
    private val PICK_IMAGE_REQUEST = 2
    private val TAG = "commentActivity"
    private val AUTOCOMPLETE_REQUEST_CODE = 1
    private lateinit var fusedLocationProviderClient : FusedLocationProviderClient

    //oggetti neccessari per la creazione di una segnalazione
    private var animal: Animale? = null
    var place: Place? = null
    private lateinit var desc: String
    var bitmap: Bitmap? = null
    private var filePath: Uri? = null
    private var latLng : MyLatLng? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_reporting_layout)
        presenter = NewReportPresenter(this)
    }



    @RequiresApi(Build.VERSION_CODES.N)
    override fun init() {

        toolbar_new_rep.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        toolbar_new_rep.setNavigationOnClickListener {
            finish()
        }

        val date = Calendar.getInstance()
        date_edit_text.setText(
                SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date.time).toString()
        )

        val dateSetListener =
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    date.set(Calendar.YEAR, year)
                    date.set(Calendar.MONTH, monthOfYear)
                    date.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    date_edit_text.setText(
                            SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date.time).toString()
                    )

                }

        date_edit_text.setOnClickListener {
            DatePickerDialog(
                    this, dateSetListener,
                    date.get(Calendar.YEAR),
                    date.get(Calendar.MONTH),
                    date.get(Calendar.DAY_OF_MONTH)
            ).show()
        }


        // Initialize Places.
        if (!Places.isInitialized())
            Places.initialize(applicationContext, BuildConfig.MAPS_API_KEY)

        placeField.setOnClickListener { v: View? ->
            // Set the fields to specify which types of place data to return.
            val fields: List<Place.Field> =
                    listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields
            )
                    .build(this)
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }


        /* SPINNER ANIMAL CHOICE */
        val spinner: Spinner = findViewById(R.id.spinner_animal_choice)
        ArrayAdapter.createFromResource(
                this,
                R.array.animals,
                R.layout.support_simple_spinner_dropdown_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.onItemSelectedListener = this
        }


        /* IMAGE */

        segna_image_item.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                    Intent.createChooser(intent, "Select Picture"),
                    PICK_IMAGE_REQUEST
            )
        }

        new_rep_current_pos.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION),123)

            }
            else{
                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
                fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                    if(it != null){
                        latLng = MyLatLng(it.latitude,it.longitude)
                        placeField.setText(latLng!!.convertLatLngToAddress(latLng!!,this))
                    }
                }.addOnFailureListener {
                    showMessage("on failure")
                }
            }


        }


        /* SAVE BUTTON */
        save_comment_button.setOnClickListener {
            val uuid : UUID = UUID.randomUUID()
            desc = description_edit_text.text.toString()
            val err = presenter.checkReportInfo(date, desc, animal, place,latLng)
            when (err) {
                1 -> showMessage(getString(R.string.newRepErr_date))
                2 -> showMessage(getString(R.string.newRepErr_desc))
                3 -> showMessage(getString(R.string.newRepErr_animal))
                4 -> showMessage(getString(R.string.newRepErr_place))
                5 -> showMessage("Place must not be empty")
                else -> {
                    if (bitmap != null) {
                        presenter.tryToAddNewImage(bitmap!!, uuid)
                    }
                    if (place != null) {
                        presenter.tryToAddNewReport(
                            Report(
                                "Autore",
                                MyLatLng(place!!.latLng!!.latitude, place!!.latLng!!.longitude),
                                date.timeInMillis,
                                description_edit_text.text.toString(),
                                uuid.toString(),
                                Stato.APERTA,
                                animal!!
                            ),
                            uuid
                        )
                    }
                    if(latLng != null){
                        presenter.tryToAddNewReport(
                            Report(
                                "Autore",
                                MyLatLng(latLng!!.lat, latLng!!.lng),
                                date.timeInMillis,
                                description_edit_text.text.toString(),
                                uuid.toString(),
                                Stato.APERTA,
                                animal!!
                            ),
                            uuid
                        )
                    }
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(
                TAG,
                "dentro a onActivityResult, codiceRequest: $requestCode e codiceResult: $resultCode"
        )
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                place = Autocomplete.getPlaceFromIntent(data!!)
                placeField.setText(place!!.name)
                Log.i(TAG, "Place: " + place!!.name + ", " + place!!.id + ", " + place!!.latLng)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Log.e("NewReportActivity", data.toString())
                val status: Status = Autocomplete.getStatusFromIntent(data!!)
                Log.i(TAG, status.statusMessage!!.toString())
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("NewReportActivity", "Action canceled")
            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data == null || data.data == null) {
                return
            }
            filePath = data.data
            val selectedPhotoUri = data.data
            try {
                selectedPhotoUri?.let {
                    if (Build.VERSION.SDK_INT < 28) {
                        bitmap = MediaStore.Images.Media.getBitmap(
                                this.contentResolver,
                                selectedPhotoUri
                        )
                    } else {
                        val source =
                                ImageDecoder.createSource(this.contentResolver, selectedPhotoUri)
                        bitmap = ImageDecoder.decodeBitmap(source)
                    }
                }
                if (bitmap != null) {
                    segna_image_item.setImageBitmap(bitmap)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent != null) {
            val animalStr = parent.getItemAtPosition(position) as String
            when (animalStr) {
                resources.getStringArray(R.array.animals)[1] -> animal = Animale.CANE
                resources.getStringArray(R.array.animals)[2] -> animal = Animale.GATTO
                resources.getStringArray(R.array.animals)[3] -> animal = Animale.TARTARUGA
                resources.getStringArray(R.array.animals)[4] -> animal = Animale.CONIGLIO
                resources.getStringArray(R.array.animals)[5] -> animal = Animale.PAPPAGALLO
                resources.getStringArray(R.array.animals)[6] -> animal = Animale.CRICETO
                resources.getStringArray(R.array.animals)[7] -> animal = Animale.CANARINO
                resources.getStringArray(R.array.animals)[8] -> animal = Animale.SERPENTE
                resources.getStringArray(R.array.animals)[9] -> animal = Animale.RAGNO
                resources.getStringArray(R.array.animals)[10] -> animal = Animale.GALLINA
                else -> null
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        animal = Animale.CANE
    }


    override fun onSuccess() {
        val returnIntent = Intent()
        setResult(RESULT_OK, returnIntent)
        finish()
    }

    override fun onFailure() {
        val returnIntent = Intent()
        setResult(-2, returnIntent)
        showMessage(getString(R.string.newRepErr_generic))
        finish()
    }


    fun showMessage(s: String) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show()
    }



}