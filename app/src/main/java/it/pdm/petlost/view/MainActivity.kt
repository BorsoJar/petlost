
package it.pdm.petlost.view


import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.google.android.material.navigation.NavigationView
import it.pdm.petlost.interfaces.MainActivityInterface
import it.pdm.petlost.model.data.MyLatLng
import it.pdm.petlost.presenter.MainActivityPresenter
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.android.synthetic.main.report_list_item_fragment.*
import java.text.SimpleDateFormat
import java.util.*
import it.pdm.petlost.R
import it.pdm.petlost.model.NotificationsService
import kotlinx.android.synthetic.main.settings_fragment.*


class MainActivity : AppCompatActivity(), MainActivityInterface.MainActivityView {

    private lateinit var presenter: MainActivityInterface.MainActivityPresenter
    lateinit var appBarConfiguration: AppBarConfiguration


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_pet_lost_navigation)
        presenter = MainActivityPresenter(this)
        presenter.tryToGetUserInfo()


    }

    private fun hasNotificationsServiceToGetEnabled():Boolean
    {
        val prefs = getSharedPreferences(getString(R.string.sharedPrefences), Context.MODE_PRIVATE)
        val userPreferences = prefs.getBoolean(getString(R.string.notification_flag_userPreferences), false)
        return (userPreferences || presenter.isAdmin())
    }

    private fun startNotificationService()
    {
        val intent = Intent(this, NotificationsService::class.java)
        this.startService(intent)
    }
    override fun initView(){

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
                                                    setOf(
                                                        R.id.mapsFragment,
                                                        R.id.reportListFragment,
                                                        R.id.myReportListFragment,
                                                        R.id.settingsFragment,
                                                        R.id.aboutFragment,
                                                        R.id.logout),
                                                    drawerLayout
                                                )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navView.getHeaderView(0).setOnClickListener {
            navController.navigate(R.id.profileFragment)
            drawerLayout.closeDrawers()
        }

        navView.menu.findItem(R.id.logout).setOnMenuItemClickListener {
            presenter.tryToLogout(applicationContext)
            true
        }

        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            supportActionBar!!.hide()
        }

    }
    override fun setUserInfoOnDrawer(name: String, surname: String, email: String) {
        (findViewById(R.id.nav_view) as NavigationView).getHeaderView(0).user_info_nav_txt.text = "$name $surname"
        (findViewById(R.id.nav_view) as NavigationView).getHeaderView(0).user_info_nav_email.text = email
    }

    override fun setUserPhotoOnDrawer(photo: Bitmap) {
        user_photo_nav.setImageBitmap(photo)
    }


    override fun returnToLoginActivity() {
        try {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
        finally {
            finish()
        }
    }

    override fun onSuccess(requestCode: MainActivityInterface.ActionCode, value: Any?) {

        when(requestCode)
        {
            MainActivityInterface.ActionCode.USER_INFO -> {
                setUserInfoOnDrawer(
                    presenter.getName(),
                    presenter.getSurname(),
                    presenter.getEmail()
                )

                if (hasNotificationsServiceToGetEnabled())
                    startNotificationService()
                if(presenter.isAdmin()){
                    val prefs = getSharedPreferences(getString(R.string.sharedPrefences), Context.MODE_PRIVATE)
                    prefs.edit().putBoolean(getString(R.string.notification_flag_isAdmin), true).apply()
                }

            }
            MainActivityInterface.ActionCode.LOGOUT -> returnToLoginActivity()
            MainActivityInterface.ActionCode.USER_PHOTO -> setUserPhotoOnDrawer(presenter.getPhoto())
        }

    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return NavigationUI.navigateUp(navController,appBarConfiguration)
    }



    override fun onFailure(requestCode: MainActivityInterface.ActionCode) {
        when(requestCode)
        {
            MainActivityInterface.ActionCode.USER_INFO -> Toast.makeText(this, getString(R.string.reqInfoErr), Toast.LENGTH_LONG).show()
            MainActivityInterface.ActionCode.LOGOUT -> Toast.makeText(this, getString(R.string.reqLogoutErr), Toast.LENGTH_LONG).show()
            MainActivityInterface.ActionCode.USER_PHOTO -> user_photo_nav.setImageResource(R.drawable.ic_baseline_camera_alt_24)
        }

    }

    override fun onSaveInstanceState(oldInstanceState: Bundle) {
        super.onSaveInstanceState(oldInstanceState)
       oldInstanceState.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.deletePresenter()
    }

}