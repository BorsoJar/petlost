package it.pdm.petlost.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import it.pdm.petlost.R
import it.pdm.petlost.interfaces.Adapter
import it.pdm.petlost.interfaces.MyReportInterface
import it.pdm.petlost.interfaces.ReportListInterface
import it.pdm.petlost.model.data.MyLatLng
import it.pdm.petlost.model.data.ReportListItem
import it.pdm.petlost.model.util.ReportListAdapter
import it.pdm.petlost.presenter.MyReportListPresenter
import it.pdm.petlost.view.MainActivity
import it.pdm.petlost.view.NewReportActivity
import kotlinx.android.synthetic.main.my_report_list_fragment.*
import kotlinx.android.synthetic.main.report_list_fragment.recycle_list_report

class MyReportItemListFragment : Fragment(R.layout.my_report_list_fragment), MyReportInterface.MyViewReport, Adapter.ItemClickInterface {

    private lateinit var presenter :  MyReportInterface.MyRepPresenter
    private lateinit var list : MutableList<ReportListItem>
    private lateinit var mActivity : FragmentActivity
    private val NEW_REPORTING_REQUEST_CODE = 1

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.let { mActivity = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpToolbar()
        presenter = MyReportListPresenter(this)

        list = presenter.getReportsData(true,) as MutableList<ReportListItem>

        updateView(list)

        my_rep_add_new_rep.setOnClickListener {
            val intent = Intent(activity, NewReportActivity::class.java)
            startActivityForResult(intent, NEW_REPORTING_REQUEST_CODE)
        }
    }

    override fun onItemClick(position: Int) {

        val date = list[position].retriveDate(list[position].data)
        val animal = list[position].anType
        val state = list[position].state
        val img = list[position].img
        val desc = list[position].desc
        val place = list[position].place

        val action = MyReportItemListFragmentDirections.actionMyReportListFragmentToItemListFragment(date,animal,state,desc,img,place,true,list[position].reportId)
        requireView().findNavController().navigate(action)

    }

    override fun updateView(reportsDataForList: List<ReportListItem>) {
        list = reportsDataForList.toMutableList()
        for(item in list)
            item.place = placeTranslater(item, requireContext())

        recycle_list_report.adapter = ReportListAdapter(list, this, requireContext())
        recycle_list_report.layoutManager = LinearLayoutManager(context)
        recycle_list_report.setHasFixedSize(true)
    }


    private fun setUpToolbar() {

        val mainActivity = mActivity as MainActivity
        val navigationView: NavigationView = mActivity.findViewById(R.id.nav_view)

        mainActivity.setSupportActionBar(toolbar_my_rep)
        val navController = NavHostFragment.findNavController(this)
        val appBarConfiguration = mainActivity.appBarConfiguration
        setupActionBarWithNavController(mainActivity,navController,appBarConfiguration)
        NavigationUI.setupWithNavController(navigationView,navController)
        toolbar_my_rep.setTitle(R.string.my_rep_title_frag)
    }

    override fun onDeleteSuccess() {
        Toast.makeText(requireContext(), "Delete Success!", Toast.LENGTH_LONG).show()
    }

    override fun onDeleteFailed() {
        Toast.makeText(requireContext(), "Delete Failed!", Toast.LENGTH_LONG).show()
    }

    override fun onPause() {
        presenter.deleteMyRepListPresenter()
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == NEW_REPORTING_REQUEST_CODE)
            list = presenter.getReportsData(true).toMutableList()
        updateView(list)

    }

    private fun placeTranslater(reportListItem: ReportListItem, context: Context) : String{
        var address : String
        var myLatLng = MyLatLng()
        myLatLng = myLatLng.stringToLatLng(reportListItem.place)
        address = myLatLng.convertLatLngToAddress(myLatLng,context)
        return address
    }

}