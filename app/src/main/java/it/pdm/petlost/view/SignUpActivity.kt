package it.pdm.petlost.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import it.pdm.petlost.R
import it.pdm.petlost.presenter.SignUpPresenter
import it.pdm.petlost.interfaces.SignUpInterface
import kotlinx.android.synthetic.main.sign_up_layout.*

class SignUpActivity : AppCompatActivity(), SignUpInterface.ViewSignUp {
    private var presenter: SignUpPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up_layout)
        presenter = SignUpPresenter(this)

//        TEST_precompilaCampi()
        signup_button.setOnClickListener{
            val error = presenter!!.checkUserInfoFormat(
                    signup_name.text.toString(),
                    signup_surname.text.toString(),
                    signup_email.text.toString(),
                    signup_password.text.toString(),
                    signup_emailConfirmation.text.toString(),
                    signup_passwordConfirmation.text.toString()
            )

            if (errorDataHandler(error))
                presenter?.tryToSign(
                        signup_name.text.toString(),
                        signup_surname.text.toString(),
                        signup_email.text.toString(),
                        signup_password.text.toString(),
                        signup_emailConfirmation.text.toString(),
                        signup_passwordConfirmation.text.toString()
                )
        }



    }

    override fun errorDataHandler(error: Int) : Boolean {
        var isDataOK = false
        when (error) {

            1 -> signup_layoutName.error = getString(R.string.accessError_emptyField_name)
            2 -> signup_layoutSurname.error = getString(R.string.accessError_emptyField_surname)
            3 -> signup_layoutEmail.error = getString(R.string.accessError_emptyField_email)
            4 -> signup_layoutPassword.error = getString(R.string.accessError_emptyField_psw)
            5 -> {
                signup_layoutEmail.error = getString(R.string.accessError_notMatching_email);
                signup_layoutEmailConfirmation.error = getString(R.string.accessError_notMatching_email)
            }
            6 -> {
                signup_layoutPassword.error = getString(R.string.accessError_notMatching_psw);
                signup_layoutPasswordConfirmation.error = getString(R.string.accessError_notMatching_psw)
            }
            7 -> signup_layoutEmail.error = getString(R.string.accessError_invalidFormat_email)
            8 -> signup_layoutPassword.error = getString(R.string.accessError_invalidFormat_psw)
            9 -> signup_layoutEmail.error = getString(R.string.accessError_emailAlreadyUsed)
            10 -> signup_button.error = getString(R.string.accessError_generic)
            else -> isDataOK = true
        }
        return isDataOK
    }

    private fun clearErrMsg() {
        signup_layoutName.error = null
        signup_layoutSurname.error = null
        signup_layoutEmail.error = null
        signup_layoutPassword.error = null
        signup_layoutEmailConfirmation.error = null
        signup_layoutPasswordConfirmation.error = null
    }

    private fun clearTextView() {
        signup_name.setText("")
        signup_surname.setText("")
        signup_email.setText("")
        signup_emailConfirmation.setText("")
        signup_password.setText("")
        signup_passwordConfirmation.setText("")
    }

    override fun onSignUpSuccess() {
        clearErrMsg()
        //clearTextView()


        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.emailConfirmation_signupDialog_title))
        builder.setMessage(getString(R.string.emailConfirmation_signupDialog_message))
        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
            returnToLoginActivity()
        }


        builder.show()


        Toast.makeText(this, "Registrazione eseguita correttamente", Toast.LENGTH_SHORT).show()
    }


    private fun returnToLoginActivity()
    {
        try {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
        finally {
            finish()
        }

    }

    override fun onSignUpFailure(messaggio: String) {
        clearErrMsg()
        //clearTextView()

        errorDataHandler(10)
    }


    private fun TEST_precompilaCampi()
    {
        signup_name.setText("Gianluca")
        signup_surname.setText("Battocchio")
        signup_email.setText("battocchio.gianluca0@gmail.com")
        signup_emailConfirmation.setText("battocchio.gianluca0@gmail.com")
        signup_password.setText("Qwerty1!")
        signup_passwordConfirmation.setText("Qwerty1!")
    }

}